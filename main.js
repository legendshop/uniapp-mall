import Vue from 'vue'
import App from './App'
import { getUa } from 'utils/utils.js';
import { appToken } from '@/utils/Cache.js';
import store from './store'
Vue.config.productionTip = false

// #ifdef H5
import VueClipboard from 'vue-clipboard2'; Vue.use(VueClipboard);// 剪切(H5端专用,app和小程序不能用这个)

import vConsole from 'vconsole';
if (process.env.NODE_ENV === 'development') {
	var VConsole = new vConsole();
}
// #endif

/* 
    引入导航栏和tabbar样式风格
*/
 import { applyStyle,initTabBarStyle } from '@/utils/colorUtils.js';
 function applyColorStyle(color,options){ applyStyle(color,options) } Vue.prototype.$applyStyle = applyColorStyle;
 Vue.prototype.$store = store
 store.dispatch('appSetting').then(res => { // 先执行一次store里的方法拿到后台的颜色设置值
     initTabBarStyle(store.state.themes.color)
 })
 store.dispatch('serverNowDate');// 先执行一次store里的方法拿到后台服务器时间与机器时间的差值
 Vue.mixin({
    onShow(){
        if (appToken.isLogin() && JSON.stringify(store.state.userInfo) == '{}') { // 如果已经登录了，并且VUEX里没有userInfo，则请求接口获取
            store.dispatch('getUserInfo');// 先执行一次store里的方法拿到后台服务器时间与机器时间的差值
        }
    },
});

import config from '@/config/config.js';
import sliceArr from 'utils/sliceArr';
require('promise.prototype.finally').shim();// 解决不登录的情况下,首页一直显示加载中却不加载数据的bug(因为低版本的浏览器axios不支持finally)
// 服务器图片
Vue.prototype.$imgUrlHead = config.photoServer;

// 静态服务器图片
Vue.prototype.$staticServer = config.staticServer;

// Vue.prototype.$sliceArr = sliceArr;

// 上传图片插件
import uploader from 'utils/uploader.js'
Vue.prototype.$Uploader = uploader;
import lsImage from '@/components/ls-app/ls-image/ls-image.vue'; Vue.component('ls-image',lsImage);// 线上图片组件

// 时间格式
import moment from 'utils/moment.js';
Vue.filter('dateformat', function(dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') { return dataStr ? moment(dataStr).format(pattern) : '暂无' })
function dateformat(dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') { return dataStr ? moment(dataStr).format(pattern) : '暂无' }
Vue.prototype.$dateformat = dateformat;// {{ item.addtime | dateformat('YYYY-MM-DD') }}


/* 跳转到传入的url地址——用法:
    传参:@click="$navigateTo('/modMy02/message/messageDet?item='+ encodeURIComponent(JSON.stringify(item)) )"
    接参:if(option.item){ const item = JSON.parse(decodeURIComponent(option.item)); } 
*/
function navigateTo(url){ uni.navigateTo({ url: url }) } Vue.prototype.$navigateTo = navigateTo;
function redirectTo(url){ uni.redirectTo({ url: url }) } Vue.prototype.$redirectTo = redirectTo;
function switchTab(url){ uni.switchTab({ url: url }) } Vue.prototype.$switchTab = switchTab;
function reLaunch(url){ uni.reLaunch({ url: url }) } Vue.prototype.$reLaunch = reLaunch;
function navigateBack(delta){ uni.navigateBack({ delta: delta || 1 }) } Vue.prototype.$navigateBack = navigateBack;

/* ios微信打开网页键盘弹起后页面上滑，导致弹框里的按钮响应区域错位，只能用强制滚动来处理(如果以后微信升级后处理了这个问题，那就可以删除IosWxPageScrollTo这个方法)
   <input type="text" @blur="$IosWxPageScrollTo()"  v-model="params.remark">
*/
function IosWxPageScrollTo(num){ 
    // #ifdef H5
    if(getUa().isWeixin && uni.getSystemInfoSync().platform == 'ios'){
        uni.pageScrollTo({ scrollTop: num || 0 });
    }
    // #endif
} 
Vue.prototype.$IosWxPageScrollTo = IosWxPageScrollTo;
function pageScrollTo(num = 0){ uni.pageScrollTo({ scrollTop: num }); } Vue.prototype.$pageScrollTo = pageScrollTo;// 滚动到屏幕某个位置，不存值就默认置顶

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
