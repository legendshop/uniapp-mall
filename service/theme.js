import { getBaseSetting } from '@/api/common/common.js'



function themeService() {
	const rgb = { r: 51, g: 51, b: 51 }
	const parmes = { theme: 'black', color:'#333333' ,rgb: rgb };
	return new Promise((resolve, reject) => {
		getBaseSetting().then(res => {
			if(res.status == 1){
				parmes.theme = res.result.theme
				parmes.color = res.result.color
				parmes.categorySetting = res.result.categorySetting
				resolve(parmes);
			} else {
				reject(parmes);
			}
		}).catch(() => {
			reject(parmes);
		})
	})
}


export default themeService

