/**
 * 支付相关的业务,比如充值,订单支付
 */
import axios from 'uni-axios';
import { payto } from '@/api/common/appPayCentre.js'; //APP支付中心-->调用支付
import { getUa } from "@/utils/utils.js";
import requests from '@/utils/request.js'
import Qs from 'qs'

//本js方法返回结果的data类型
const resultDataType = {
    FORM: "form",
    JSON: "json"
};

//定义本js支持的支付方式
const payType = {
    WXPAY: "wxpay", //微信支付
    ALIPAY: "alipay", //支付宝支付
    APPLEIAP: "appleiap", //apple支付
    SIMULATE: "simulate", //模拟支付
    FULL_PAY: "fullpay", //全额预存款支付
}

//定义后台支付接口支持的支付方式
const apiPayType = {
    ALP: "ALP", //支付宝支付
    WX_PAY: "WX_PAY", //微信公众号支付 jsapi
    WX_H5_PAY: "WX_H5_PAY", //微信H5支付
    WX_APP_PAY: "WX_APP_PAY", //微信APP支付
    WX_MP_PAY: "WX_MP_PAY", //微信小程序支付
    SIMULATE: "SIMULATE", //商城模拟支付
    FULL_PAY: "FULL_PAY", //全额预存款支付
}

//定义后台接口支付单据类型
const subSettlementType = {
    ORDER: "USER_ORDER", //普通商品订单支付
    PREPAID_RECHARGE: "USER_PRED_RECHARGE", //余额充值
    AUCTION_DEPOSIT: "AUCTION_DEPOSIT", //拍卖保证金支付
    PRESELL_ORDER: "PRESELL_ORDER_PAY" //预售订单支付
}

//定义后台接口支付来源
const apiPaySource = {
    WAP: "WAP",
    WEIXIN: "WX_MP",
    APP: "APP"
};

//定义本js方法的错误对象
const myError = {
    //未知错误
    unknown: {
        code: "UNKNOWN",
        msg: "系统异常, 支付失败, 请重试或联系客服人员!"
    },
    //不支持
    noSupport: {
        code: "NO_SUPPORT",
        msg: "暂不支持您选择的支付, 请尝试选择其他支付!"
    },
    //重复支付
    repeition: {
        code: "REPETITION",
        msg: "您已经发起过支付了, 请勿重复支付!"
    },
    //取消支付
    cancel: {
        code: "CANCEL",
        msg: "您取消了支付!"
    },
    //网络异常
    network: {
        code: "NETWORK",
        msg: "网络异常, 请检查您的网络设置!"
    }
}





//H5浏览器支付
const h5Pay = {
    //支付类型常量, 调用者可以通过他作为支付类型参数传入调用支付方法
    payType: payType,

    /**
     * 判断是否是支持的支付类型
     * @param {*} payTypeValue 支付类型
     */
    isSupportPayTypes(payTypeValue) {
        if (!payTypeValue) {
            return false;
        }

        for (var key in payType) {
            if ((payTypeValue = payType[key])) {
                return true;
            }
        }
        return false;
    },

    /**
     * 普通订单支付
     * @param {*} payTypeValue 支付方式
     * @param {*} prePayTypeVal 余额支付方式(1:预付款  2：金币, 默认预存款)
     * @param {*} passwordCallback 余额是否确认支付
     * @param {*} subNumbers 订单号数组
     */
    orderPay(payTypeValue, prePayTypeVal, passwordCallback, subNumbers) {
        return this.pay(
            subSettlementType.ORDER,
            payTypeValue,
            prePayTypeVal,
            passwordCallback,
            subNumbers,
            false,
            null
        );
    },

    /**
     * 充值支付
     * @param {*} payTypeValue 支付方式
     * @param {*} subNumbers 充值单号数组
     */
    recharge(payTypeValue, subNumbers) {
        console.log('chongzhi h5')
        return this.pay(
            subSettlementType.PREPAID_RECHARGE,
            payTypeValue,
            '',
            0,
            subNumbers,
            false,
            ''
        );
    },

    /**
     * 拍卖保证金支付, 暂不实现
     */
    auctionDepositPay() {},

    /**
     * 预售支付
     */
    presellOrderPay() {},

    /**
     * 通用支付
     * @param {*} subSettlementTypeValue 交易单据类型
     * @param {*} payTypeValue 支付方式
     * @param {*} prePayTypeVal 余额支付方式(1:预付款  2：金币, 默认预存款)
     * @param {*} passwordCallback 预存款支付是否完成确认付款
     * @param {*} subNumbers 订单号数组
     * @param {*} presell 是否为预售订单，true：是，false：否
     * @param {*} payPctType 预售(拍卖)支付:尾款/定金
     */
    pay( subSettlementTypeValue,payTypeValue,prePayTypeVal,passwordCallback,subNumbers,presell,payPctType ) {
        return new Promise((resolve, reject) => {
            let apiPayType = _method.getApiPaytype(payTypeValue);
            let apiPaySource = _method.getApiPaySource(payTypeValue);

            let payParams = {
                payTypeId: apiPayType,
                prePayTypeVal: prePayTypeVal,
                passwordCallback: passwordCallback,
                subNumbers: subNumbers,
                paySource: apiPaySource,
                presell: presell,
                payPctType: payPctType
            };

            //如果是微信公众号支付, 要带openID; openId不应该是由前端来传的, 临时这样写的
            if (apiPayType && payParams.payTypeId == apiPayType.WX_PAY) {
                payParams.openId = localStorage.getItem("openId");
            }

            let result = {};
            payto(payParams, subSettlementTypeValue).then(res => {
                if (res.status == 1) {
                    console.log('走了h5')
                    result.success = true;
                    result.data = res.result;
                    result.dataType = resultDataType.FORM;
                    result.isApp = false;
                    resolve(result);
                } else {
                    result.success = false;
                    result.errorCode = myError.unknown.code;
                    result.msg = res.msg;
                    resolve(result);
                }
            })
            .catch(error => {
                result.success = false;
                result.errorCode = myError.network.code;
                result.msg = myError.network.code;
                reject(result);
            });
        });
    }
};



//小程序支付
const mpPay = {

    //支付类型常量, 调用者可以通过他作为支付类型参数传入调用支付方法
    payType: payType,

    /**
     * 判断是否是支持的支付类型
     * @param {*} payTypeValue 支付类型
     */
    isSupportPayTypes(payTypeValue) {
        if (!payTypeValue) {
            return false;
        }

        for (var key in payType) {
            if (payTypeValue = payType[key]) {
                return true;
            }
        }
        return false;
    },

    /**
     * 普通订单支付
     * @param {*} payTypeValue 支付方式
     * @param {*} prePayTypeVal 余额支付方式(1:预付款  2：金币, 默认预存款)
     * @param {*} passwordCallback 余额是否确认支付
     * @param {*} subNumbers 订单号数组
     */
    orderPay(payTypeValue, prePayTypeVal, passwordCallback, subNumbers) {
        return this.pay(subSettlementType.ORDER, payTypeValue, prePayTypeVal, passwordCallback, subNumbers);
    },

    /**
     * 充值支付
     * @param {*} payTypeValue 支付方式
     * @param {*} subNumbers 充值单号数组
     */
    recharge(payTypeValue, subNumbers) {

        return this.pay(subSettlementType.PREPAID_RECHARGE, payTypeValue, '', 0, subNumbers, false, '');
    },

    /**
     * 拍卖保证金支付, 暂不实现
     */
    auctionDepositPay() {

    },

    /**
     * 预售支付, 暂不实现
     */
    presellOrderPay() {

    },

    /**
     * 通用支付
     * @param {*} subSettlementTypeValue 交易单据类型
     * @param {*} payTypeValue 支付方式
     * @param {*} prePayTypeVal 余额支付方式(1:预付款  2：金币, 默认预存款)
     * @param {*} passwordCallback 预存款支付是否完成确认付款
     * @param {*} subNumbers 订单号数组
     * @param {*} presell 是否为预售订单，true：是，false：否
     * @param {*} payPctType 预售(拍卖)支付:尾款/
     * 
     */

    pay(subSettlementTypeValue, payTypeValue, prePayTypeVal, passwordCallback, subNumbers, presell, payPctType) {
        // pay(subSettlementTypeValue, payTypeValue, prePayTypeVal, passwordCallback, subNumbers) {
        return new Promise((resolve, reject) => {
            let apiPayType = _method.getApiPaytype(payTypeValue); //支付方式 微信支付 支付宝支付
            let apiPaySource = _method.getApiPaySource(payTypeValue); //支付来源 app支付 web支付
            let payParams = {
                payTypeId: apiPayType,
                // payTypeId:'WX_APP_PAY' ,
                prePayTypeVal: prePayTypeVal,
                passwordCallback: passwordCallback,
                subNumbers: subNumbers,
                paySource: apiPaySource,
                presell: presell,
                payPctType: payPctType
            }
            let result = {};
            console.log(payParams)
            payto(payParams, subSettlementTypeValue).then(res => {
                console.log(res)
                if (res.status == 1) {
                    if (payTypeValue == payType.SIMULATE || payTypeValue == payType.FULL_PAY) { //是否是模拟支付或预存款支付
                        result.success = true;
                        result.dataType = resultDataType.JSON;
                        //prepayResult 是 notify url, 需要异步请求一下
						console.log(encodeURI(res.result.prepayResult))
						console.log(33333333333)
                        axios.get(encodeURI(res.result.prepayResult)).then(notifyRes => {
                            if (notifyRes.data == "ok") { // notify说明成功
								console.log(88888888)
								console.log(notifyRes)
                                result.success = true;
                                result.data = res.result.payInfo; //商户系统的支付信息(我们的支付接口)
                            } else {
                                result.success = false;
                                result.errorCode = myError.unknown.code;
                                result.msg = myError.unknown.msg;
                            }
                            resolve(result);
                        }).catch(e => {
                            console.log("请求 notify url 失败! e: %o", e);
                            result.success = false;
                            result.errorCode = myError.unknown.code;
                            result.msg = myError.unknown.msg;
                            reject(result);
                        });

                    } else {
                        let prepayResult = res.result.prepayResult
                        let payParamss

                        if(prepayResult.startsWith('http')){//如果是混合支付，预存款够了，却选择了微信(这时后台只调用预存款支付，不会调起微信)，则后台返回的是一个带https的成功网址，这时就要走下面这个逻辑
                            axios.get(encodeURI(prepayResult)).then(notifyRes => {
                                if (notifyRes.data == "ok") { // notify说明成功
                                    result.success = true;
                                    result.data = res.result.payInfo; //商户系统的支付信息(我们的支付接口)
                                } else {
                                    result.success = false;
                                    result.errorCode = myError.unknown.code;
                                    result.msg = myError.unknown.msg;
                                }
                                resolve(result);
                            }).catch(e => {
                                console.log("请求 notify url 失败! e: %o", e);
                                result.success = false;
                                result.errorCode = myError.unknown.code;
                                result.msg = myError.unknown.msg;
                                reject(result);
                            });
                            
                            return
                        }
                        
                        if (payTypeValue == payType.WXPAY) { //如果是微信支付, 需要把预支付结果转为JSON
                            prepayResult = JSON.parse(prepayResult);
							console.log(prepayResult)
							payParamss=prepayResult
                            // payParamss = {
                            //     appid: prepayResult.appid,
                            //     package: prepayResult.package, // 固定值，以微信支付文档为主
                            //     partnerid: prepayResult.partnerid,
                            //     prepayid: prepayResult.prepayid,
                            //     timestamp: prepayResult.timestamp,
                            //     noncestr: prepayResult.noncestr,
                            //     sign: prepayResult.sign,
                            // }
                        } else {
                            payParamss = prepayResult
                        }

                        let payInfo = res.result.payInfo;
                        // #ifdef APP-PLUS
                        uni.requestPayment({
                            'provider': payTypeValue,
                            orderInfo: payParamss,
                            success(res) {
                                console.log("调用App支付成功: %o", res);
                                result.success = true;
                                result.data = payInfo;
                                result.dataType = resultDataType.JSON;
                                resolve(result);
                            },
                            fail(res) {
                                console.log("调用App支付失败: %o", res);
                                result.success = false;
                                if (res.errMsg == 'requestPayment:fail cancel') {
                                    result.errorCode = myError.cancel.code;
                                    result.msg = myError.cancel.msg;
                                } else {
                                    result.errorCode = myError.unknown.code;
                                    result.msg = myError.unknown.msg;
                                }
                                resolve(result);
                            }
                        });
                        // #endif
                        // #ifndef APP-PLUS
                        // payment
                        uni.requestPayment({
                            'timeStamp': prepayResult.timeStamp,
                            'nonceStr': prepayResult.nonceStr,
                            'package': prepayResult.package,
                            'signType': prepayResult.signType,
                            'paySign': prepayResult.paySign,
                            success(res) {
                                console.log("调用小程序支付成功: %o", res);
                                result.success = true;
                                result.data = payInfo;
                                result.dataType = resultDataType.JSON;
                                resolve(result);
                            },
                            fail(res) {
                                console.log("调用小程序支付失败: %o", res);
                                result.success = false;
                                if (res.errMsg == 'requestPayment:fail cancel') {
                                    result.errorCode = myError.cancel.code;
                                    result.msg = myError.cancel.msg;
                                } else {
                                    result.errorCode = myError.unknown.code;
                                    result.msg = myError.unknown.msg;
                                }

                                resolve(result);
                            }
                        });
                        // #endif

                    }

                } else {
                    result.success = false;
                    result.errorCode = myError.unknown.code;
                    result.msg = res.msg;
                    resolve(result);
                }
            }).catch(error => {

                console.log(error);

                result.success = false;
                result.errorCode = myError.network.code;
                result.msg = myError.network.code;
                reject(result);
            });
        });
    }
}


/* 一些私有方法 */
const _method = {

    /**
     * 通过本js定义的支付方式获取后台接口对应的支付方式
     * @param {*} payTypeValue 本js定义的支付方式
     */
    getApiPaytype(payTypeValue) {
        var value = null;
        switch (payTypeValue) {

            //支付宝支付
            case payType.ALIPAY:
                value = apiPayType.ALP;
                break;

                //微信支付
            case payType.WXPAY:
                // #ifdef MP-WEIXIN
                value = apiPayType.WX_MP_PAY;
                // #endif
                // #ifdef H5
                //否则为浏览器
                if (getUa().isWeixin && payTypeValue=='wxpay') {
                    // 如果是微信登录H5,并选择了微信支付
                    value = apiPayType.WX_PAY;
                } else {
                    value = apiPayType.WX_H5_PAY
                }
                // #endif
                // #ifdef APP-PLUS
                value = apiPayType.WX_APP_PAY;
                // #endif
                break;

                //apple支付, 后台暂不支持applepay
            case payType.APPLEIAP:

                break;

                //模拟支付
            case payType.SIMULATE:
                value = apiPayType.SIMULATE;
                break;
        }

        return value;
    },

    /**
     * 获取后台接口的支付来源
     */
    getApiPaySource(payTypeValue) {
        let value

        // #ifdef APP-PLUS
        value = apiPaySource.APP;
        // #endif
        // #ifdef MP-WEIXIN
        value = apiPaySource.WEIXIN;
        // #endif
        // #ifdef H5
            //否则为浏览器
            if ( getUa().isWeixin && payTypeValue=='wxpay' ) {
                // 如果是微信登录H5,并选择了微信支付
                value = "WEIXIN";
            } else {
                value = apiPaySource.WAP;
            };
        // #endif

        return value;
    }
};

// #ifdef H5
let platformPayto = h5Pay;
// #endif
// #ifndef H5
let platformPayto = mpPay;
// #endif


export default platformPayto;
