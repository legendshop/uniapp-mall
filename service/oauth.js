import { appToken } from '@/utils/Cache.js';
import { oauthApi } from '@/api/common/common.js';
import { getUa } from '@/utils/utils';
import config from '@/config/config.js';



// 普通版本的用户认证
const generalServiceImpl = {
	/**
	 * 普通登录
	 * @param {*} username 登录名
	 * @param {*} password 密码
	 */
	login(username,password,passportIdKey) {
		const result = {};
		const authUser = {
			loginName:username,
			password,
            passportIdKey,
			platform: _method.getPlatform(),
            // code:'0000'
		}
		return new Promise((resolve, reject) => {
			oauthApi.login(authUser).then(res => {
   				if (res.status == 1) {
                    appToken.setToken(res.result,'login');
					result.success = true;
					result.data = appToken.getToken();
					resolve(result);
				} else {
					 result.success = false;
					 result.msg = res.msg;
					 resolve(result);
				}
			}).catch(() => {
				result.success = false;
				result.msg = '网络请求异常!';
				reject(result);
			})
		})
	},
    /**
     * 第三方登录(H5的方法写在login页面上,APP才用到此方法)
     * @param {*} provider 第三方登录的id，qq：QQ登录，weixin：微信登录，sinaweibo：新浪微博登录
     */
    oauthLogin(provider) {
        // 扩展API加载完毕，现在可以正常调用扩展API
        const result = {};
        let params = {}
        return new Promise((resolve, reject) => {
            plus.oauth.getServices(
                function(services) {
                    console.log('services: ' + JSON.stringify(services));
                    let auth = '';
                    services.forEach(element => {
                        if (element.id == provider) {
                            auth = element;
                        }
                    });
                    auth.logout(
                        function(e) {},
                        function(e) {}
                    ); // 注销登录授权认证,这个可以删掉，但删掉后，要隔一段时间后才能让你选择另一个账号登录
                    auth.login(
                        function(res) {
                            if (res.target) {
                                params = {
                                    type: provider,
                                    platform: _method.getPlatform(),
                                    accessToken: res.target.authResult.access_token,
                                    openId: res.target.authResult.openid
                                };
                                if (res.target.authResult) {
                                    if (res.target.authResult.refresh_token) {
                                        params.refreshToken = res.target.authResult.refresh_token;
                                    }
                                }
                                if (res.target.userInfo) {
                                    if (res.target.userInfo.headimgurl) {
                                        params.avatarUrl = res.target.userInfo.headimgurl;
                                    }
                                    if (res.target.userInfo.gender) {
                                        params.gender = res.target.userInfo.gender;
                                    }
                                    if (res.target.userInfo.nickname) {
                                        params.nickName = res.target.userInfo.nickname;
                                    }
                                    if (res.target.userInfo.city) {
                                        params.city = res.target.userInfo.city;
                                    }
                                    if (res.target.userInfo.country) {
                                        params.country = res.target.userInfo.country;
                                    }
                                    if (res.target.userInfo.province) {
                                        params.province = res.target.userInfo.province;
                                    }
                                }
                            }
                            // console.log(params);
                            oauthApi.oauthLogin(params).then(res => {
                                if (res.status == 1) {
                                    appToken.setToken(res.result,'oauthLogin');
                                    result.success = true;
                                    result.token = appToken.getToken();
                                    result.msg = '登录成功！';
                                    result.status = 1;
                                    resolve(result);
                                } else if (res.status == 2) {
                                    result.passportIdKey = res.result;
                                    result.platform = _method.getPlatform();
                                    result.success = true;
                                    result.msg = '认证通过，需要绑定账号！';
                                    result.status = 2;
                                    console.log('result: ' + JSON.stringify(result));
                                    resolve(result);
                                } else {
                                    result.success = true;
                                    result.msg = res.msg;
                                    console.log('res: ' + JSON.stringify(res));
                                    resolve(result);
                                }
                            }).catch(e => {
                                console.error('第三方', e);
                                result.success = false;
                                result.msg = '网络请求异常!';
                                result.networkErr = true; // 网络请求异常就跳至首页
                                resolve(result);
                            });
                        },
                        function(error) {
                            console.log(error);
                            result.success = false;
                            result.msg = '登录失败:' + error.message.split('http')[0];
                            resolve(result);
                        }
                    );
                },
                function(error) {
                    result.success = false;
                    result.msg = '获取授权失败:' + error.message.split('http')[0];
                    resolve(result);
                }
            );
        });
    },
	/**
	   * 快捷登录
	   * @param {*} phone 手机号码
	   * @param {*} smsCode 短信验证码
	*/
	quickLogin(phone,smsCode,passportIdKey) {
		const result = {};
		
		const params = {
			mobile: phone,
			mobileCode: smsCode,
			deviceId: '',
			platform: _method.getPlatform(),
            passportIdKey
		};
		
		return new Promise((resolve, reject) => {
			oauthApi.quickLogin(params).then(res => {
				// console.log(res);
				if (res.status == 1) {
                    appToken.setToken(res.result,'login');
                    result.success = true;
                    result.data = appToken.getToken();
                    resolve(result);
				} else {
					 result.success = false;
					 result.msg = res.msg;
					 resolve(result);
				}
			}).catch(() => {
				result.success = false;
				result.msg = '网络请求异常!';
				reject(result);
			})
		})
	},
	
	// 退出登录
	logout() {
		const result = {};
		return new Promise((resolve, reject) => {
			oauthApi.logout().then(res => {
				if (res.status == 1) {
					appToken.clearToken();
                    
					result.success = true;
					resolve(result);
				} else {
					result.success = false;
					result.msg = res.msg;
					resolve(result);
				}
			}).catch(error => {
				result.success = false;
				result.msg = '网络请求异常!';
				reject(result);
			})
		})
	}
	
}


// oath2.0认证
const oauth2ServiceImpl = {
    /**
     * 普通登录
     * @param {*} loginName 登录名
     * @param {*} password 密码
     */
    login(loginName, password){
        const result = {};

        const params = {
            'grant_type': 'password',
            'username': loginName,
            'password': password,
            'scope': 'server'
        };
        // 如果是微信公众号登录  就提供openId
        if(getUa().isWeixin) {
          params.openId = passport.getPassportIdKey()
        }
        // 免密登录
        // let params = {
        //   grant_type: 'client_credentials'
        // };

        return new Promise((resolve, reject) => {
            oauthApi.oauth2(params).then(res => {
                if (res.error) {
                    result.success = false;
                    result.msg = '用户认证失败!';
                    resolve(result);
                } else {
                    appToken.setToken(res,'oauth2');
                    result.success = true;
                    result.data = appToken.getToken();
                    resolve(result);
                }
            }).catch(e => {
                result.success = false;
                result.msg = '网络异常!';

                reject(result);
            });
        });
    },

    /**
     * 快捷登录
     * @param {*} phone 手机号码
     * @param {*} smsCode 短信验证码
     */
    quickLogin(phone, smsCode){
        const result = {};
        const params = {
            'mobile': 'SMS@' + phone,
            'code': smsCode,
            'grant_type':'mobile'
        };

        return new Promise((resolve, reject) => {
            oauthApi.quickOauth2(params).then(res => {
                console.log(res);
                if (res.error) {
                    result.success = true;
                    result.msg = res.error;
                    resolve(result);
                } else {
                    appToken.setToken(res,'oauth2');
                    result.success = true;
                    result.data = appToken.getToken();
                    resolve(result);
                }
            }).catch(e => {
                result.success = true;
                result.msg = '网络异常!';
                reject(result);
            });
        });
    },
    /**
     * 检查头肯是否有效请求
     * @param {*} token
     */
    checkToken(token){
        const result = {};
        const params = {
            'token': token,
        };
        return new Promise((resolve, reject) => {
            oauthApi.checkToken(params).then(res => {
                if (res.error) {
                    result.data = res
                    result.success = false;
                    result.msg = '检查请求头失败!';
                    resolve(result);
                } else {
                    result.data = res
                    result.success = true;
                    result.msg = '请求头验证通过!';
                    resolve(result);
                }
            }).catch(e => {
                console.log(e);
                result.success = false;
                result.msg = '请求头验证不通过!';
                reject(result);
            });
        });
    },
    /**
     * 刷新token
     * @param {*} refreshToken 刷新token
     */
    refreshToken(refreshToken){
        const result = {};

        const params = {
            'grant_type': 'refresh_token',
            'refresh_token':refreshToken
        };

        return new Promise((resolve, reject) => {
            oauthApi.oauth2(params).then(res => {
                console.log(res);
                if (res.error) {
                    result.success = true;
                    result.msg = '刷新token失败!';

                    resolve(result);
                } else {
                    appToken.setToken(res,'oauth2');
                    result.success = true;
                    result.data = appToken.getToken();
                    resolve(result);
                }
            }).catch(e => {
                result.success = true;
                result.msg = '网络异常!';
                reject(result);
            });
        });
    },

  /**
   * APP第三方登录
   * @param {*} provider 第三方登录的id，qq：QQ登录，weixin：微信登录，sinaweibo：新浪微博登录
   */
  oauthLogin(provider){
        // 扩展API加载完毕，现在可以正常调用扩展API
        const result = {};
        let params = {}
        return new Promise((resolve, reject) => {
            plus.oauth.getServices(
                function(services) {
                    console.log('services: ' + JSON.stringify(services));
                    let auth = '';
                    services.forEach(element => {
                        if (element.id == provider) {
                            auth = element;
                        }
                    });
                    auth.logout(function(e) {}, function(e) {}); // 注销登录授权认证,这个可以删掉，但删掉后，要隔一段时间后才能让你选择另一个账号登录
                    auth.login(
                        function(res) {
                            console.log(res);
                            if (res.target) {
                                // console.log(provider);
                                // console.log(res.target.authResult.access_token);
                                // console.log(res.target.authResult.openid);
                                // console.log(_method.getPlatform());
                                params = {
                                    type: provider,
                                    platform: _method.getPlatform(),
                                    accessToken: res.target.authResult.access_token,
                                    openId: res.target.authResult.openid
                                };
                                if (res.target.authResult) {
                                    if (res.target.authResult.refresh_token) {
                                        params.refreshToken = res.target.authResult.refresh_token;
                                    }
                                }
                                if (res.target.userInfo) {
                                    if (res.target.userInfo.headimgurl) {
                                        params.avatarUrl = res.target.userInfo.headimgurl;
                                    }
                                    if (res.target.userInfo.gender) {
                                        params.gender = res.target.userInfo.gender;
                                    }
                                    if (res.target.userInfo.nickname) {
                                        params.nickName = res.target.userInfo.nickname;
                                    }
                                    if (res.target.userInfo.city) {
                                        params.city = res.target.userInfo.city;
                                    }
                                    if (res.target.userInfo.country) {
                                        params.country = res.target.userInfo.country;
                                    }
                                    if (res.target.userInfo.province) {
                                        params.province = res.target.userInfo.province;
                                    }
                                }
                            }
                            console.log(params);
                            oauthApi.oauthLogin(params).then(res => {
                                console.log(2222222);
                                console.log(res);
                                if (res.status == 1) {
                                    appToken.setToken(res,'oauth2')
                                    result.success = true;
                                    result.token = appToken.getToken();
                                    result.msg = '登录成功！';
                                    result.status = 1;
                                    resolve(result);
                                } else if (res.status == 2) {
                                    result.passportIdKey = res.result;
                                    result.platform = _method.getPlatform();
                                    result.success = true;
                                    result.msg = '认证通过，需要绑定账号！';
                                    result.status = 2;
                                    console.log('result: ' + JSON.stringify(result));
                                    resolve(result);
                                } else {
                                    result.success = true;
                                    result.msg = res.msg;
                                    console.log('res: ' + JSON.stringify(res));
                                    resolve(result);
                                }
                            }).catch(e => {
                                console.error('第三方', e);
                                result.success = false;
                                result.msg = '网络请求异常!';
                                result.networkErr = true; // 网络请求异常就跳至首页
                                resolve(result);
                            });
                        },
                        function(error) {
                            console.log(error);
                            result.success = false;
                            result.msg = '登录失败:' + error.message.split('http')[0];
                            resolve(result);
                        }
                    );
                },
                function(error) {
                    result.success = false;
                    console.log(1111111);
                    console.log(error);
                    result.msg = '获取授权失败:' + error.message.split('http')[0];
                    resolve(result);
                }
            );
        });
    },
    /**
     * 退出登录
     */
    logout(){
        const result = {};

        return new Promise((resolve, reject) => {
            oauthApi.logout().then(res => {
            	if (res.status == 1) {
                    // 微服务的退出，必须是要先调用/p/logout接口成功后再调用/oauth/logout接口，不然就会出现小程序退出后，再登录时，是直接登录而不能进入bindingPhone页面的bug
                    oauthApi.oauth2Logout({ accessToken:appToken.getToken().accessToken }).then(res2 => {
                        if (res2.error) {
                            result.success = false;
                            result.msg = res2.msg;
                            resolve(result);
                        } else {
                            appToken.clearToken();
                    
                            result.success = true;
                            resolve(result);
                        }
                    }).catch(error => {
                        result.success = false;
                        result.msg = '网络请求异常!';
                        reject(result);
                    });
                    
                    // appToken.clearToken();
            		// result.success = true;
            		// resolve(result);
            	} else {
            		result.success = false;
            		result.msg = res.msg;
            		resolve(result);
            	}
            }).catch(error => {
            	result.success = false;
            	result.msg = '网络请求异常!';
            	reject(result);
            })
        });
    }
}


// 私有的方法 
const _method = {
	// 获取登录平台来源
	getPlatform() {
		let platform = ''
		
		// #ifdef H5
			platform = 'H5'
		// #endif
		
		// #ifdef MP-WEIXIN
			platform = 'MP'
		// #endif
		
		
		
		// #ifdef APP-PLUS
		if (uni.getSystemInfoSync().platform == 'ios') {
			platform = 'IOS'
		} else {
			platform = 'ANDROID'
		}
		// #endif
		
		// console.log(platform)
		
		return platform
	},
}

let oauthService = '';
if (!config.isOauth2) { // 如果不是微服务版本(即是正常版本)
    oauthService = generalServiceImpl
}else{ // 如果是微服务版本
    oauthService = oauth2ServiceImpl
}

export default oauthService
