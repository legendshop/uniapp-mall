import request from '@/utils/request'

export const getOpenShopInfo = params => request.post('/p/getOpenShopInfo', params); // 获取商家入驻信息

export const openShop = params => request.post('/p/openShop', params); // 商家入驻

export const saveOpenShopInfo = params => request.post('/p/saveOpenShopInfo', params); // 保存商家入驻信息

export const shopAgreement = params => request.post('/p/shopAgreement', params); // 获取商家入驻协议
