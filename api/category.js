import request from '@/utils/request.js'

/*
    分类页面api
*/


// 获取所有品牌信息
export const brandList = () => request.post('/brandList', {}, { isShowLoadding:true })

// 获取所有分类信息
export const category = () => request.post('/category', {}, { isShowLoadding:true })

// 分类搜索接口
export const prodlist = (params) => request.post('/appSearch/prodlist' , params)
