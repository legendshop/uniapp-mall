import request from '@/utils/request'

// 客服相关api

// 获取客服会话聊天界面数据
export const dialogue = params => request.post('/p/im/dialogue', params);

// 获取指定用户的聊天记录
export const chatrecords = params => request.get('/p/im/chatrecords', params,{ isShowToast:false });


// 与店铺相关的订单
export const getorderShop = params => request.post('/p/im/order', params,{ isShowToast:false });


// 获取当前正在咨询的商品或订单信息
export const getConsulting = params => request.post('/p/im/getConsulting', params,{ isShowToast:false });
