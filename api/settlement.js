import request from '@/utils/request'

/*
    下单购买流程api
*/
const config = {
  headers: {
    'Content-Type': 'multipart/form-data' // 之前说的以表单传数据的格式来传递fromdata
  }
};

// 提交订单的详情
export const orderDetails = params => request.post('/p/app/orderDetails', params)



// 提交订单获取优惠券
export const orderCouponsPageForApp = params => request.post('/p/app/coupon/orderCouponsPageForApp', params)


// 提交订单
export const submitOrder = params => request.post('/p/app/submitOrder', params)


// 付款页面获取数据
export const payData = params => request.post('/p/app/payData', params)


// 立即购买参数检查
export const checkBuyNow = params => request.post('/p/app/checkBuyNow', params)


// 确认并核对门店订单信息

export const storeOrderDetails = params => request.post('/p/storeOrderDetails', params)

// 获取距离最近的门店
export const getNearStore = params => request.post('/getNearStore', params)

// 提交门店订单
export const submitStoreOrder = params => request.post('/p/submitStoreOrder', params)


// 获取门店列表
export const queryStore = params => request.post('/queryStore', params)


// 更换绑定的门店
export const updateShopStore = params => request.post('/p/updateShopStore', params)


