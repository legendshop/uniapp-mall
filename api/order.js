import request from '@/utils/request'

/*
    订单相关api
*/

// 订单列表
export const orderLisg = params => request.post('/p/myOrder', params,{ isShowLoadding:true })

// 删除订单
export const removeOrder = params => request.post('/p/removeOrder', params)

// 提醒发货
export const sendSiteMsg = params => request.post('/p/sendSiteMsg', params)

// 确认收货
export const orderReceive = params => request.post('/p/orderReceive', params)

// 取消订单
export const cancleOrder = params => request.post('/p/cancleOrder', params)

// 查看物流
export const orderLogistics = params => request.post('/p/orderLogistics', params, { isShowToast:false })

// 查看物流(积分商品)
export const integralLogistics = params => request.post('/p/app/integral/order/logistics', params)

// 订单详情
export const myOrderDetail = params => request.post('/p/myOrderDetail', params)

// 订单流水获取订单信息
export const successOrderDetail = params => request.post('/p/successOrderDetail', params)

// 再来一单
export const buyAgain = params => request.post('/p/buyAgain', params)

// 订单申请售后前 判断该订单是否是拼团的团长免单订单
export const isMergeGroupHeadFreeSub = params => request.post('/p/isMergeGroupHeadFreeSub', params)
