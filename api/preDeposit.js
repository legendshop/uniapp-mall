import request from '@/utils/request'

export const preDeposit = () => request.post('/p/app/predeposit'); // 预存款主页

export const getAvailablePredeposit = () => request.get('/p/app/predeposit/getAvailablePredeposit'); // 获取可提现金额

export const pdCashLogDetail = params => request.post('/p/app/predeposit/pdCashLogDetail',params); // 账户明细详情

export const pdCashLogList = params => request.post('/p/app/predeposit/pdCashLogList',params,{ isShowLoadding:true }); // 账户明细

export const recharge = params => request.post('/p/app/predeposit/recharge',params); // 充值

export const rechargeDetail = params => request.post('/p/app/predeposit/rechargeDetail',params); // 充值记录详情

export const successOrderDetail = params => request.post('/p/app/predeposit/successOrderDetail',params); // 充值记录详情

export const rechargeList = params => request.post('/p/app/predeposit/rechargeList',params); // 充值记录

export const toRecharge = params => request.post('/p/app/predeposit/toRecharge/' + params.pdrSn); // 去充值

export const withdrawalsDetail = params => request.post('/p/app/predeposit/withdrawalsDetail',params); // 提现记录详情

export const withdrawalsList = params => request.post('/p/app/predeposit/withdrawalsList',params,{ isShowLoadding:true }); // 提现记录

export const withdrawalsSave = params => request.post('/p/app/predeposit/withdrawalsSave',params); // 提现

