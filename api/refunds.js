/* 退货退款 api */

import request from '@/utils/request.js'


const config = {
  headers: {
    'Content-Type': 'multipart/form-data' // 之前说的以表单传数据的格式来传递fromdata
  }
};


// 申请退款  --  待收货状态
export const refundApply = params => request.post('/p/refund/apply', params)

// 申请订单项仅退款，退款退货(单个商品)
export const refundReturn = params => request.post('/p/itemRefund/apply', params)

// 保存订单项 "仅退款" 申请(单个商品)
export const itemRefund = params => request.post('/p/itemRefund/save', params)

// 保存订单项 "退货退款" 申请(单个商品)
export const itemReturn = params => request.post('/p/itemReturn/save', params)


// 提交退款申请
export const refundSave = params => request.post('/p/refund/save', params)

// 我的订单项详情
export const myOrderItemDetail = params => request.post('/p/myOrderItemDetail', params)


// 申请记录
export const subRefundReturnList = params => request.post('/p/subRefundReturnList', params,{ isShowLoadding:true })

// 查看退款详情
export const subRefundReturnDetail = params => request.post('/p/subRefundReturnDetail', params)

// 用户退货给商家-填写物流
export const returnGoods = params => request.post('/p/returnGoods', params)

// 撤销申请
export const candelAction = params => request.post('/p/refund/candelAction', params)

// 修改申请
export const editAction = params => request.post('/p/refund/editAction', params)

// 回调的保存修改 - 保存修改申请
export const submitAgainApply = params => request.post('/p/refund/submitAgainApply', params)


// 上传图片
export const uploadImg = params => request.post('/p/upload', params, config)

// 删除图片
export const deletePic = params => request.post('/p/deletePic', params)
