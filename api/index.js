import request from '@/utils/request'

// console.log(request)

// 商城首页
export const ShoppingIndex = () => request.get('/index')

// 获取分类下的商品
export const categoryProd = (prames) => request.post('/categoryProd', prames)

// 获取店铺商品分组
export const shopCategory = (prames) => request.post('/shopCategoryProd', prames)

// 获取商品分组商品
export const groupProd = (prames) => request.post('/groupProd', prames)



// 获取海报页内容
export const showPosterPage = (prames) => request.post('/showPosterPage', prames)
