/*
    秒杀相关api
*/

import request from '@/utils/request'


// 获取拼团专区的banner图列表
export const seckillbanners = () => request.post(`/app/seckills/banners`);


// 获取秒杀列表时间、状态信息
export const seckillStatusInfo = params => request.post(`/app/seckillList/statusInfo`, params);


// 获取秒杀列表
export const seckillList = params => request.post(`/app/seckillList`, params);


// 获取秒杀活动详情
export const seckillDetail = params => request.post('/app/seckills/' + params.seckillId , params);


// 执行秒杀
export const seckillExecute = params => request.post('/p/app/' + params.seckillId + '/execute' , params);


// 取秒杀成功信息
export const seckillSuccess = params => request.post('/p/app/' + params.seckillId + '/' + params.prodId + '/' + params.skuId + '/seckillSuccess' , params);


// // 提交秒杀订单的详情
export const seckillOrderDetails = params => request.post(`/p/app/seckill/orderDetails`, params);


// // 提交订单的详情
export const seckillSubmitOrder = params => request.post(`/p/app/seckill/submitOrder`, params);


// 查询我的秒杀记录
export const secKillQuery = params => request.get(`/p/secKill/query`, params);


// 删除秒杀订单
export const secKillDeleteRecord = params => request.get(`/p/secKill/deleteRecord/` + params.id);
