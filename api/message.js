/* 我的信息相关 api */


import request from '@/utils/request'



// 获取最新的系统广播
export const news = () => request.post('/p/msg/system/new')


// 获取系统通知列表
export const systemList = () => request.post('/p/msg/system/list')

// 获取普通消息列表
export const newList = params => request.post('/p/msg/list',params,{ isShowLoadding:true })

// 删除普通消息
export const deleteList = params => request.post('/p/msg/delete',params)

// 获取普通消息详情数据
export const msgDet = params => request.post('/p/msg/load',params)

// 获取系统通知的详情数据
export const systemLoad = params => request.post('/p/msg/system/load',params)

// 删除系统通知
export const systemDelete = params => request.post('/p/msg/system/delete',params)

// 获取客服会话列表
export const imMessage = params => request.post('/p/imMessage',params,{ isShowLoadding:true })
