import request from '@/utils/request'


export const checkUserNameExist = params => request.post('/isUserNameExist', params); // 判断用户名是否存在

export const checkPhoneExist = params => request.post('/isPhoneExist', params); // 判断手机号码是否存在

export const userReg = params => request.post('/userReg', params); // 用户注册

export const forgetPwd = params => request.post('/forgetPwd', params); // 忘记密码  找回密码

export const registerAgreement = params => request.post('/registerAgreement', params); // 注册协议

export const isCompanyVer = params => request.post('/isCompanyVer', params); // 企业税务号是否正确

export const userCompanyReg = params => request.post('/userCompanyReg', params); // 企业用户注册
