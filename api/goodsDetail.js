import request from '@/utils/request'


export const productDetail = params => request.post('/productDetail/' + params.prodId,params,{ isShowLoadding:true }); // 获取商品详情

export const isExistsFavorite = params => request.post('/isExistsFavorite', params); // 是否已收藏商品

export const productComments = params => request.post('/productComments', params); // 获取商品评论

export const productCommentNumbers = params => request.post('/productCommentNumbers/' + params); // 获取商品评论数量


export const commentReplyList = params => request.post('/commentReplyList', params); // 获取评论的回复列表


export const prodCommentDetail = params => request.post('/prodCommentDetail', params); // 获取评论详情


export const replyComment = params => request.post('/p/replyComment', params); // 回复评价（评论）


export const replyParent = params => request.post('/p/replyParent', params); // 回复别人的回复


export const updateUsefulCounts = params => request.post('/p/updateUsefulCounts', params); // 给评论点赞


export const getProdParams = params => request.get('/getProdParams/' + params.prodId); // 给评论点赞

export const bySkuActivity = params => request.post('/bySkuActivity' , params); // 根据sku返回参与了哪些活动


// 获取生成普通商品海报相关的数据
export const getProdPosterData = params => request.post('/getProdPosterData', params);

export const isSupportStore = params => request.post('/isSupportStore' , params); // 是否支持门店自提

export const serviceShow = params => request.get('/serviceShow' , params); // 根据商品id获取服务说明




