import request from '@/utils/request'

export const getUserInfo = params => request.post('/p/userInfo', params); // 账户设置

export const editLoginPwd = params => request.post('/p/changePwd', params); // 修改登录密码

export const editPhone = params => request.post('/p/updateMobile', params); // 修改手机号码

export const editNickname = params => request.post('/p/updateNickName', params); // 修改昵称

export const updateSignature = params => request.post('/p/updateSignature', params); // 修改签名

export const editPayPwd = params => request.post('/p/updatePaymentPassword', params); // 修改支付密码

export const editPortrait = params => request.post('/p/updatePortrait', params); // 修改头像

export const editName = params => request.post('/p/updateRealName', params); // 修改姓名

export const editSex = params => request.post('/p/updateSex', params); // 修改性别

export const editBirthday = params => request.post('/p/updateBirthDate', params); // 修改生日

export const uploadFile = params => request.post('/p/upload', params); // 上传图片

export const checkPhone = params => request.post('/p/validateUserMobile', params); // 校验原手机号
