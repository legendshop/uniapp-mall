/*
    秒杀相关api
*/

import request from '@/utils/request'


// 获取拼团专区的banner图列表
export const presellBanners = () => request.post(`/app/presell/banners`);


// 获取预售商品列表
export const presellList = params => request.post(`/app/presell/list`, params);


// 获取预售商品详情
export const presellDetail = params => request.post('/app/presell/views/' + params.presellId, params);


// 提交预售订单的详情
export const presellOrderDetails = params => request.post(`/p/app/presell/orderDetails`, params);


// 提交订单的详情
export const presellSubmitOrder = params => request.post(`/p/app/presell/submitOrder`, params);


// 预售检查下单
export const checkPresellOrder = params => request.post(`/p/app/presell/checkPresell`, params);
