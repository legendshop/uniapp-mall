import request from '@/utils/request'


export const applyPromoter = params => request.post('/p/appPromoter/applyPromoter', params); // 我的推广


export const submitPromoter = params => request.post('/p/appPromoter/submitPromoter', params); // 提交申请


export const myPromoter = params => request.post('/p/myPromoter', params); // 我的分销


export const loadRule = params => request.get('/loadRule', params); // 分销规则


export const toApplyCash = params => request.post('/p/toApplyCash', params); // 提取当前可提现金额


export const applyCash = params => request.post('/p/applyCash', params); // 处理申请提现


export const myCommisDetailList = params => request.post('/p/myCommisDetailList', params,{ isShowLoadding:true }); // 佣金明细


export const withdrawList = params => request.post('/p/withdrawList', params,{ isShowLoadding:true }); // 提现记录


export const myPromoterUser = params => request.post('/p/myPromoterUser', params,{ isShowLoadding:true }); // 我推广的好友


export const getPromoteWxACode = params => request.post('/p/getPromoteWxACode'); // 我推广的好友
