import request from '@/utils/request'

/*
    种草社区页面api
*/


// 获取文章列表
export const grassList = params => request.post(`/grassList`, params,{ isShowLoadding:true });


// 获取文章详情
export const grassInfo = params => request.post(`/grassInfo`, params,{ isShowLoadding:true });


// 发表评论(发现文章页面)
export const saveGrassComme = params => request.post(`/p/saveGrassComme`, params);


// 点赞(种草文章页面)
export const grassThum = params => request.post(`/p/grassThum`, params);


// 获取文章评论列表
export const grassCommentList = params => request.post(`/grassCommentList`, params);


// 新增或修改文章
export const saveGrassArticle = params => request.post(`/p/saveGrassArticle`, params);


// 获取标签列表
export const grassLabelList = params => request.post(`/grassLabelList`, params);


// 获取用户购买过的商品列表
export const grassUserProdList = params => request.post(`/grassUserProdList`, params);


// 关注用户
export const grassConcern = params => request.post(`/p/grassConcern`, params);


// 获取相关文章列表
export const grassRelatedList = params => request.post(`/grassRelatedList`, params);


// 获取作者相关文章列表
export const grassWriterList = params => request.post(`/grassWriterList`, params);


// 获取个人详情
export const grassWriterInfo = params => request.post(`/grassWriterInfo`, params);


// 获取作者关注或粉丝列表
export const grassConcernList = params => request.post(`/grassConcernList`, params);


// 删除文章
export const deleteGrassArticle = params => request.post(`/deleteGrassArticle`, params);
