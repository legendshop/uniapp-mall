import request from '@/utils/request'

/*
    购物车相关api
*/

// 加入购物车
export const addShopBuy = params => request.post('/p/addShopBuy', params)


// 购物车列表
export const shopCart = params => request.post('/p/shopCart', params,{ isShowLoadding:true })


// 更改购物车选中状态(单选)
export const changeBasketSts = (basketId,params) => request.post('/p/changeBasketSts/' + basketId , params)


// 批量更改购物车选中状态(多选)
export const batchChgBasketSts = params => request.post('/p/batchChgBasketSts', params,{ isShowLoadding:true })


// 查看获取商品所有sku选择
export const selectSkus = params => request.post('/selectSkus/' + params)


// 删除购物车数据
export const deleteShopCart = params => request.post('/p/deleteShopCart', params)


// 批量删除购物车商品
export const deleteShopCarts = params => request.post('/p/deleteShopCarts', params)


// 更改购物车数量
export const changeShopCartNum = params => request.post('/p/changeShopCartNum', params)


// 清空购物车失效商品
export const clearInvalid = () => request.get('/p/clearInvalid')


// 更改购物车中商品选中的sku
export const changeSelectedSku = params => request.post('/p/changeSelectedSku', params)


// 用户的购物车商品数
export const cartCount = params => request.post('/cart/count', params)
