// import configfix from '@/config/configfix';
import request from '@/utils/request';

// let extendConfig = {

//     //基础认证
//     headers: {
//         "authorization": configfix.appSecret,
//         // "Content-Type": "multipart/form-data"
//     }
// };

const oauthApi = {

    // 普通的用户账号密码登录
    login: params => request.post('/login', params),

    // 普通的快捷登录
    quickLogin: params => request.post('/smsLogin', params),

    // 微信小程序登录
    weixinMpLogin: params => request.post('/thirdlogin/weixin/mp/callback', params),
	
    // 微信小程序绑定手机号
    bindPhone: params => request.post('/bindPhone', params),

    // 退出登录
    logout: params => request.post('/p/logout', params),

    // oauth2.0的账号密码登录, 微服务
    // oauth2: params => request.post("/oauth/token", params, extendConfig),

    // oauth2.0的账号密码登录, 微服务
    // quickOauth2: params => request.post("/oauth/quickToken", params, extendConfig),

    // oauth2.0, 刷新token, 微服务
    // oauth2RefreshToken: params => request.post("/oauth/refreshToken", params, extendConfig),

    // oauth2退出登录
    // oauth2Logout: params => request.post("/oauth/logout", params),


}

export default oauthApi;
