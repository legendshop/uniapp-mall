import request from '@/utils/request.js'

/*
    发现页面api
*/


// 获取文章列表
export const discoverList = params => request.post(`/discoverList`, params,{ isShowLoadding:true });


// 获取文章详情
export const discoverInfo = params => request.post(`/discoverInfo`, params,{ isShowLoadding:true });


// 发表评论(发现文章页面)
export const saveDiscoverComme = params => request.post(`/p/saveDiscoverComme`, params);


// 点赞(发现文章页面)
export const discoverThum = params => request.post(`/p/discoverThum`, params);


// 获取文章评论列表
export const discoverCommentList = params => request.post(`/discoverCommentList`, params);
