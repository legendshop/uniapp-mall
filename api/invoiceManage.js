import request from '@/utils/request'

/*
    发票管理
*/
const config = {
  headers: {
    'Content-Type': 'multipart/form-data' // 之前说的以表单传数据的格式来传递fromdata
  }
};

// 更新发票并设置为默认
export const save = params => request.post('/p/app/invoice/save', params)


// 发票信息翻页
export const invoicePage = params => request.post('/p/app/invoicePage', params,{ isShowLoadding:true })


// 加载发票信息
export const loadInvoice = params => request.post('/p/app/loadInvoice', params)


// 设置发票为默认
export const setInvoiceCommon = params => request.post('/p/app/setInvoiceCommon', params)


// 删除发票
export const invoiceDelete = params => request.post('/p/app/invoiceDelete', params)

