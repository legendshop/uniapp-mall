import request from '@/utils/request.js'

/*
  搜索相关api
  */

// 热搜词
export const hotSearch = () => request.post('/appSearch/hotSearchList');


// 搜索商品列表
export const prodlist = (params) => request.post('/appSearch/prodlist', params, { isShowLoadding: true });


export const groupProdList = (params) => request.post('/groupProdList', params, { isShowLoadding: true });
