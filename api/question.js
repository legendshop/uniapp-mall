import request from '@/utils/request'

const questionApi = {

    question: params => request.get('/help/newsCategory', params), // 常见问题

    questionList: params => request.get('/help/news', params), // 常见问题列表
    
    questionDetail: params => request.get('/help/newsDetail', params), // 常见问题详情
}

export { questionApi }
