import request from '@/utils/request'


export const shopHead = params => request.post('/shop/index/' + params.shopId); // 店铺首页


export const hotProd = params => request.post('/shop/hotProd/' + params.shopId); // 店铺精选好货


export const newProdList = params => request.post('/shop/newProdList/' + params.shopId); // 店铺上新


export const prodCategory = params => request.post('/shop/prodCategory/' + params.shopId); // 店铺商品分类


export const prodList = params => request.post('/shop/prodList',params); // 店铺全部商品


export const decorate = params => request.post('/shop/decorate/' + params.shopId, params, { isShowToast:false }); // 店铺首页装修


export const shopCategoryProd = params => request.post('/shopCategoryProd', params); // 店铺首页装修

export const businessMessage = params => request.get('/shop/business/message' , params); // 根据店铺id获取营业执照信息








