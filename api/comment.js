import request from '@/utils/request'

/*
    评论相关api
*/    

// 获取评论页面数据
export const publish = params => request.post(`/p/comment/publish`,params);

// 获取追加评论页面数据
export const append = params => request.post(`/p/comment/append/`,params);

// 我的评论
export const userComments = params => request.post('/p/userComments', params,{ isShowLoadding:true });

// 追加评论
export const addComm = params => request.post('/p/app/comments/addComm', params);


// 发表评论
export const commentsSave = params => request.post('/p/app/comments/save', params);
