import request from '@/utils/request'

export const myIntegral = () => request.post('/p/app/integral/myIntegral'); // 获取积分中心数据

export const getRule = params => request.post('/p/app/integral/getRule', params); // 获取积分规则数据

export const integralOrderList = params => request.post('/p/app/integral/orderList', params,{ isShowLoadding:true }); // 获取积分订单列表

export const integralDetail = params => request.post('/p/app/integral/integralDetail', params,{ isShowLoadding:true }); // 获取积分明细列表

export const integralOrderCancel = params => request.post(`/p/app/integral/orderCancel/${params.orderSn}`); // 取消积分订单

export const integralOrderDel = params => request.post(`/p/app/integral/orderDel/${params.orderSn}`); // 删除积分订单

export const integralOrderReceive = params => request.post(`/p/app/integral/integralOrderReceive/${params.orderSn}`); // 确认收货

export const pointsDetail = params => request.post('/app/integral/view', params); // 获取积分商品详情

export const integralIndex = params => request.post('/app/integral/integralIndex', params,{ isShowLoadding:false }); // 获取积分商品首页

export const integralCouponList = params => request.post('/app/integral/integralCouponList', params,{ isShowLoadding:true }); // 获取积分优惠券列表

export const hotIntegralProdList = params => request.post('/app/integral/hotIntegralProdList', params,{ isShowLoadding:true }); // 获取热门兑换列表

export const commendIntegralProdList = params => request.post('/app/integral/commendIntegralProdList', params,{ isShowLoadding:true }); // 获取精品优选列表

export const checkExchange = params => request.post('/p/app/integral/checkExchange', params); // 检查兑换积分商品

export const goExchange = params => request.post('/p/app/integral/goExchange', params); // 去兑换积分商品

export const submitOrder = params => request.post('/p/app/integral/submitOrder', params); // 提交积分兑换订单

export const integralOrderDetail = params => request.post(`/p/app/integral/orderDetail/${params.orderSn}`); // 积分订单详情

export const integralInfo = params => request.post('/p/app/integral/integralInfo', params); // 获取积分明细详情




