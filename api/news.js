import request from '@/utils/request'


export const NewsList = params => request.post('/headlines', params); // 新闻列表

export const NewsDetail = params => request.post(`/headlines/${params.id}`); // 新闻详情
