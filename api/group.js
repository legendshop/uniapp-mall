import request from '@/utils/request'

// 获取团购详情
export const groupDetail = params => request.post('/app/group/views', params);

// 检查团购下单
export const checkGroupOrder = params => request.post('/p/app/group/checkGroupOrder', params);

// 团购提交订单的详情
export const orderDetails = params => request.post('/p/app/group/orderDetails', params);

// 获取团购列表
export const groupList = params => request.post('/app/group/list', params);

// 团购分类
export const categoryList = params => request.post('/app/group/categoryList', params);
