/**
 * 公共服务接口
 */
import request from '@/utils/request.js'
import configfix from '@/config/configfix.js';

const extendConfig = {
    // 基础认证
    headers: {
        'Authorization': configfix.appSecret,
    },
    isShowToast:false
};

const config = {
    headers: {
      'Content-Type': 'multipart/form-data' // 之前说的以表单传数据的格式来传递fromdata
    }
};

/**
 * 删除图片
 * @param picPath 要删除的图片文件名数组
 */
const deletePic = params => request.post('/p/deletePic',params);

/**
 * 上传图片
 * @param files 文件对象数组
 */
const upload = params => request.post('/p/upload',params , config);

/**
 * 获取支付方式
 */
const queryPayType = () => request.post('/queryPayType');

/**
 * 发送短信验证码
 * @param mobile 手机号码, 当来源是提现时不需要传
 * @param userName 用户名, 当source是注册时, 才需要传
 * @param source 发送来源, reg: 注册, forget: 忘记密码, updatePhone: 修改手机号, withdraw: 提现, updatePasswd: 修改登录密码, updatePayPasswd: 修改支付密码, self: 当前登录用户发送短信验证码， other: 其他
 */
const sendSMSCode = params => request.post('/sendSMSCode', params); // 公共服务接口-->发送短信验证码

const appSetting = () => request.get('/appSetting');

const serverNowDate = () => request.get('/serverNowDate');// 获取服务器当前的时间(用来取代new Date().getTime()，因为机器上的时间并不是服务器上的时间)

const oauthApi = {
  // 普通的用户账号密码登录
  login: params => request.post('/login', params),

  // 普通的快捷登录
  quickLogin: params => request.post('/smsLogin', params),

  // 第三方登录
  oauthLogin: params => request.post('/thirdlogin/' + params.type + '/app/callback', params, { isShowToast:false }),

  // 绑定手机号
  bindPhone: params => request.post('/bindPhone', params),

  // 退出登录
  logout: params => request.post('/p/logout', params),

  // oauth2.0的账号密码登录, 微服务(刷新token也是这个接口)
  oauth2: params => request.post('/oauth/token', params, extendConfig),
  
  // oauth2.0的检查token是否有效, 微服务
  checkToken: params => request.post('/oauth/check_token', params, extendConfig),

  // oauth2.0的手机短信登录, 微服务
  quickOauth2: params => request.post('/oauth/mobile/token/sms?mobile=' + params.mobile + '&code=' + params.code + ' &grant_type=mobile', {}, extendConfig),
  // quickOauth2: params => request.post('/oauth/quickToken', params, extendConfig),

  // oauth2.0, 刷新token, 微服务
  // oauth2RefreshToken: params => request.post('/oauth/refreshToken', params, extendConfig),

  // oauth2退出登录
  oauth2Logout: params => request.post('/oauth/logout', params)
}

export {
  deletePic,
  upload,
  queryPayType,
  sendSMSCode,
  appSetting,
  oauthApi,
  serverNowDate
}
