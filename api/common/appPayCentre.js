/**
 * 支付中心相关的接口
 */
import request from '@/utils/request'

export const payto = (params,type) => request.post('/p/payment/payto/' + type , params); // APP支付中心-->调用支付

export const userPredeposit = params => request.post('/p/payment/userPredeposit/' + params.payType , params,{ isShowToast:false }); // APP支付中心-->使用预付款或金币

export const payTypes = () => request.post('/p/payTypes'); // APP支付中心-->payTypes

export const payError = p => request.get('/p/payment/payError',p); // APP支付中心-->payError

export const returnPay = p => request.get('/p/payment/returnPay',p); // APP支付中心-->payResult

export const getOrderActualTotal = p => request.post('/p/getOrderActualTotal',p); // APP支付中心-->payResult




