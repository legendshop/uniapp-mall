import request from '@/utils/request'

/*
    地址管理
*/
const config = {
  headers: {
    'Content-Type': 'multipart/form-data' // 之前说的以表单传数据的格式来传递fromdata
  }
};

// 地址列表
export const addressList = params => request.post('/p/addressList', params)


// 设置默认收货地址
export const defaultaddress = params => request.post('/p/defaultaddress', params)


// 加载收货地址
export const loadAddress = params => request.post('/p/loadAddress', params)



// 保存或更新收货地址
export const saveAddress = params => request.post('/p/saveAddress', params)


// 删除收货地址
export const deladdress = params => request.post('/p/deladdress', params)


// 加载所有的省份
export const loadProvinces = params => request.post('/common/loadProvinces', params)



// 加载省份的所有城市
export const loadCities = params => request.post('/common/loadCities/' + params.provinceid)


// 加载城市的所有区域
export const loadAreas = params => request.post('/common/loadAreas/' + params.loadAreas)

