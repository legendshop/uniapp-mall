/*
    拼团相关api
*/

import request from '@/utils/request'


// 获取拼团专区的banner图列表
export const fightbanners = () => request.post(`/app/mergeGroup/banners`);


// 获取拼团列表
export const fightLists = params => request.post(`/app/mergeGroup/list`, params);

// 获取拼团活动详情
export const fighGroupdetail = params => request.post(`/app/mergeGroup/detail`, params);


// 获取正在进行的团列表
export const fightaddUserLists = params => request.post(`/app/mergeGroup/addUser/list`, params);


// 提交订单的详情
export const orderDetails = params => request.post(`/p/app/mergeGroup/orderDetails`, params);


// 提交拼团订单
export const submitOrder = params => request.post(`/p/app/mergeGroup/submitOrder`, params);


// 获取已开团详情
export const groupDetail = params => request.post(`/app/mergeGroup/groupDetail`, params);


// 检测拼团
export const checkGroupOrder = params => request.post(`/p/app/mergeGroup/checkGroupOrder`, params);

// 获取邀请好友拼团海报相关的数据
export const getInviteJoinGroupPosterData = params => request.post(`/getInviteJoinGroupPosterData`, params);


