import request from '@/utils/request'


export const getGoodsCollection = params => request.post('/p/favorite/query', params,{ isShowLoadding:true }); // 获取商品收藏列表

export const getShopCollection = params => request.post('/p/favoriteshop/query', params); // 获取店铺收藏列表

export const deleteProdCollect = params => request.post('/p/favorite/delete', params); // 取消商品收藏
