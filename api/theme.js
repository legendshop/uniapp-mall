/**
 * 专题活动相关接口
 */
import request from '@/utils/request';

/**
 * 获取专题活动列表
 * @param {*} params 请求参数 {}
 */
const getThemeList = params => request.post('/theme/list', params);

/**
 * 获取专题活动详情
 * @param {*} params 请求参数 {}
 */
const getThemeDetail = params => request.post('/theme/detail', params);

export {
    getThemeList,
    getThemeDetail
}
