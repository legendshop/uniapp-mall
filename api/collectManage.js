// 点击关注或收藏时，收藏或取消收藏店铺的方法
import request from '@/utils/request'


export const collection = (isCollection,shopId) => request.post(isCollection == true ? `/p/shop/addFavoriteShop/${shopId}` : `/p/shop/cancelFavoriteShop/${shopId}`); // 收藏店铺/取消收藏店铺


export const favoriteGoods = shopId => request.post(`/p/favorite/save`,shopId); // 收藏商品(单个)


export const cancel = params => request.post('/p/favorite/cancel', params); // 取消商品收藏(单个)


export const favoriteGoodsDel = shopId => request.post(`/p/favorite/delete`,shopId); // 删除收藏商品(可多个)


export const favoriteGoodsDels = shopId => request.post(`/p/favorite/saves`,shopId); // 删除收藏商品(可多个)

// 收藏或取消收藏商品
export const saveOrCancel = params => request.post(`/p/favorite/saveOrCancel`,params);
