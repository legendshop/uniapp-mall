import request from '@/utils/request'


export const receive = params => request.post('/p/app/coupon/newReceive/' + params.couponId); // 领取优惠券


export const coupon = params => request.post('/p/myCoupon', params,{ isShowLoadding:true }); // 优惠券列表


export const activateCoupon = params => request.post('/p/app/activateCoupon', params); // 激活优惠券或红包



export const myRedPack = params => request.post('/p/myRedPack', params,{ isShowLoadding:true }); // 红包列表



export const couponCenter = params => request.post('/app/couponCenter', params,{ isShowLoadding:true }); // 领券中心



export const couponProdList = params => request.post('/couponProdList', params,{ isShowLoadding: true }); // 获取优惠劵对应的商品.



export const redpackProdList = params => request.post('/redpackProdList', params,{ isShowLoadding: true }); // 获取红包对应的商品.



export const couponDetailApi = params => request.post('/couponDetail/' + params.couponId); // 优惠券详情

