import request from '@/utils/request'


export const history = params => request.post('/p/my/visitHistory', params,{ isShowLoadding:true }); // 我的足迹

export const clearHistory = params => request.post('/p/my/clearVisitHistory', params); // 清空足迹
