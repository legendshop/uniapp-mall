const floorMixin = {
	data() {
		return {
			
		};
	},
	methods: {
		// 楼层装修的页面跳转
		jumpPage(params) {
		    if (params.action) {
		        if (params.action.type == 'prodDetail') {
		            // 去商品详情
		            uni.navigateTo({ url: `/commonModules/goodsDetail/goodsDetail?goodsInfoId=${params.action.target}` })
		        } else if (params.action.type == 'prodGroup') {
		            // 商品分组-  去搜索列表
		            uni.navigateTo({ url: `/commonModules/search/prodGroupList?prodGroupId=${params.action.target}` })
		        } else if (params.action.type == 'category') {
		            uni.navigateTo({ url: `/commonModules/search/searchList?categoryId=${params.action.target}` })
		        } else if (params.action.type == 'prodList') {
                    if (params.action.subType == 'shopCategory') {
                        uni.navigateTo({ url: `/commonModules/search/searchList?shopCategoryId=${params.action.target}` })
                    }else{
                        uni.navigateTo({ url: `/commonModules/search/searchList?categoryId=${params.action.target}` })
                    }
		        } else if (params.action.type == 'url') {
		            // 自定义url
		            const url = params.action.target;
		            if (url.indexOf('http') != -1 || url.indexOf('https') != -1) {
                        // #ifdef H5
                        window.location.href = url;
                        // #endif
                        // #ifndef H5
                        uni.navigateTo({ url: `/pages/webview/webview?url=${url}` })
                        // #endif
		            } else if (url == '#') {
                        uni.showToast({ title:'该功能暂未上线, 敬请期待~', icon:'none' });
			            } else {
		                uni.navigateTo({
                            url: url,
                            success(s){
                               // console.log(url);
                            },
                            fail(e){
                                // console.log(e);
                                uni.showToast({ title: '这是一个错误的链接！', duration: 2000, icon: 'none' })
                                return
                            }
                        })
		            }
		        } else if (params.action.type == 'marketing') {
		            // 营销活动
		            const target = params.action.target;
		            let url = '';
		            if (target == 'fighGroup') {
		                url = '/marketingModules/fight/fightList';
		            } else if (target == 'JoinGroup') {
		                url = '/marketingModules/group/groupList';
		            } else if (target == 'presell') {
		                url = '/marketingModules/presell/presellList';
		            } else {
		                url = '/marketingModules/seckill/seckillList';
		            }
		            uni.navigateTo({ url: url })
		        } else if (params.action.type == 'method') {
		            // 常用功能
		            const target = params.action.target;
		            let url = '';
		            if (target == 'home' || target == 'car' || target == 'class' || target == 'my') {
                        const status = { 'home':'/pages/index/index', 'car':'/pages/cart/cart', 'class':'/pages/category/category','my':'/pages/my/my' };
                        url = status[target]
		                uni.switchTab({ url: url });
		            } else {
                        const status = ({ 'coupon':'/walletModules/myCoupon/myCoupon', 'myMsg':'/personalModules/message/message', 'keep':'/personalModules/collection/collection' });
                        url = status[target]
		                uni.navigateTo({ url: url })
		            }
		        } else if (params.action.type == 'page') {
		            uni.navigateTo({ url: `/marketingModules/webPage/webPage?pageId=${params.action.target}&title=${params.action.name}` })
		        }
		    }
		},
	}
}


export default floorMixin
