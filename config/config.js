// 当前环境
let env = '';

env = process.env.NODE_ENV === 'development' ? 'prod' : 'prod';
// env = process.env.NODE_ENV === 'development' ? 'prodDev' : 'prod';
// env = process.env.NODE_ENV === 'development' ? 'oauth2' : 'oauth2';
const configs = {
    prodDev: {
        shareUrl: 'http://mdiamonds.legendshop.cn/',
        server: 'http://userapp.legendshop.cn',
        photoServer: 'http://legendshop-diamonds.oss-cn-shenzhen.aliyuncs.com/',
        staticServer: 'http://legendshop-front-resource.oss-cn-shenzhen.aliyuncs.com/miniprogram/',
        imServer: 'ws://diamondsim.legendshop.cn',
        AmapKey: '',
    },
    prod: {
        shareUrl: 'https://mdiamonds.legendshop.cn/',
        server: 'https://userapp.legendshop.cn',
        photoServer: 'https://legendshop-diamonds.oss-cn-shenzhen.aliyuncs.com/',
        staticServer: 'https://legendshop-front-resource.oss-cn-shenzhen.aliyuncs.com/miniprogram/',
        imServer: 'wss://diamondsim.legendshop.cn',
        AmapKey: '',
    },
    oauth2: { // 微服务(本地dev)
        shareUrl: 'http://192.168.0.19:8099/',
        server: 'http://legendshop.51vip.biz', // 本地
        photoServer: 'http://legendshop.51vip.biz:3005/photoserver/photo/', // 图片服务器地址
        staticServer: 'https://legendshop-front-resource.oss-cn-shenzhen.aliyuncs.com/miniprogram/',
        imServer: 'ws://192.168.0.19:2397',
        
        // 上面192.168.0.19映射出来的地址(小程序发布时有用，小程序只能在调试模式下运行)
        // shareUrl: 'http://legendshop.51vip.biz:8100/',//映射的192.168.0.19的地址
        // server: 'http://legendshop.51vip.biz:9999', //映射的192.168.0.19:9999的地址
        // photoServer: 'http://legendshop.51vip.biz:9997/photoserver/photo/', // 图片服务器地址
        // staticServer: 'https://legendshop-front-resource.oss-cn-shenzhen.aliyuncs.com/miniprogram/',
        // imServer: 'ws://legendshop.51vip.biz:9998',
        
        AmapKey: '',
        isOauth2: true,// 用来判断是否是微服务
    },
}

const config = configs[env];

export default config
