// prod生产环境
const config = {
        shareUrl: 'https://mdiamonds.legendshop.cn/',
        server: 'https://userapp.legendshop.cn',
        photoServer: 'https://legendshop-diamonds.oss-cn-shenzhen.aliyuncs.com/',
        staticServer: 'https://legendshop-front-resource.oss-cn-shenzhen.aliyuncs.com/miniprogram/',
        imServer: 'wss://diamondsim.legendshop.cn',
        AmapKey: 'b67650d214590281b2ab8e413473553d',
        companyName:'',// 公司名称。privacyPolicy《用户服务协议和隐私政策》里公司全称，不传值就默认是：广州朗尊软件科技有限公司
        copyright: '', // 软件名称。不设值的话默认是：朗尊电商
    }
    
// 微服务(本地dev)
// const config ={
//         shareUrl: "http://192.168.0.19/",
//         //server: "http://192.168.0.19:9999", 
//         server: 'http://legendshop.51vip.biz', //本地
//         //photoServer: "http://192.168.0.148:3005/photoserver/photo/", //图片服务器地址
//         photoServer: 'http://legendshop.51vip.biz:3005/photoserver/photo/', // 图片服务器地址
//         staticServer: "https://legendshop-front-resource.oss-cn-shenzhen.aliyuncs.com/miniprogram/",
//         imServer: "ws://192.168.0.19:2397",
//         AmapKey: "b67650d214590281b2ab8e413473553d",
//         isOauth2: true,//用来判断是否是微服务
//         closeDiscovery:false,//屏蔽种草功能
//         companyName:'',// 公司名称。privacyPolicy《用户服务协议和隐私政策》里公司全称，不传值就默认是：广州朗尊软件科技有限公司
//         copyright: '', // 软件名称。不设值的话默认是：朗尊电商
//     }
