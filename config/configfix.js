import config from '@/config/config.js';
// 以下是从config.js分享出来的固定的配置

var configfix = {
    Version:'V2.2.9.01',// 版本号：第一和第二个数字是指当前版本(第一个数字是大版本，第二个数字每年+1)，第三个数字是指当前月份，第四个数字是指当前月份里的第几次更新
    companyName:config.companyName ? config.companyName : '',// 公司名称，privacyPolicy《用户服务协议和隐私政策》里公司全称，不传值就默认是：广州朗尊软件科技有限公司
    copyright: config.copyright ? config.copyright : '', // 软件名称。不设值的话默认是：朗尊电商
    excludeTokenURIs:  ['/login', '/smsLogin', '/reg'],
    excludeLoginURIs:['/login', 'thirdLoginResult', 'forgetPsw','register'],
}
if (config.isOauth2) { // 如果是微服务
    const getway = {
        need: true, // 是否需要网关
        prefix: '/portalapp', // 默认大一统的业务服务网关前缀
        pay: '/portalpay', // 支付服务使用的网关前缀
        oauthPrefix: '/oauth2', // oauth2.0认证服务前缀
        basicPrefix: '/basic', // 基本的业务网关前缀
        userPrefix: '/user', // 用户服务的网关前缀
        prodPrefix: '/prod', // 商品服务的网关前缀
        orderPrefix: '/order', // 订单服务的网关前缀
        cartPrefix: '/cart', // 购物车服务的网关前缀
        excludeURIs: ['/user/getUserBaseInfo'] // 排除的url
    }
    configfix['getway'] = getway// 微服务版本, 网关相关配置
    configfix['appSecret'] = 'Basic YXBwOmFwcA=='// auth2.0要用的, 申请授权时要放在header参数
}

export default configfix
