export default {
    region: '获取中',// 地区
    addressDetail: '',// 详细地址
    lat: '',
    lng: '',
    stausBarHeight:'',// 手机状态栏高度:
    wxMenuBut:'',// 微信小程序胶囊按钮信息(占位符wxMenuBut.placeholder该值为胶囊最左边距离屏幕最右边缘的距离，单位：px):
    timeDiff:0,// 服务器上的当前时间与机器上的当前时间的时间差
    userInfo:{},// 用来接收userInfo接口获取到的res.result
    themes: { // 颜色
        theme: 'black',
        color: '#333333',
        rgb: { r: 51, g: 51, b: 51 }
    }
}
