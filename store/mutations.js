import Vue from 'vue';

export default {
    // 地址
    setRegion(state, region) {
      state.region = region
    },
    // 详细地址
    setaddressDetail(state , addressDetail) {
      state.addressDetail = addressDetail
    },
    setLat(state,lat) {
      state.lat = lat
    },
    setlng(state,lng) {
      state.lng = lng
    },
    setThemes(state,{ themes }) {
        state.themes = themes
    },
    setStausBarHeight(state,stausBarHeight) {
      state.stausBarHeight = stausBarHeight
    },
    setWxMenuBut(state,wxMenuBut) {
      state.wxMenuBut = wxMenuBut
    },
    setTimeDiff(state,timeDiff) {
      state.timeDiff = timeDiff
    },
    setUserInfo(state,userInfo) {
      state.userInfo = userInfo
    },
}

// 用法:this.$store.commit('setRegion', parmas);
