import { appSetting } from '@/api/common/common.js'
import { serverNowDate } from '@/api/common/common.js'
import { getUserInfo } from '@/api/account.js';
export default {
    // 请求主题颜色
    async appSetting({ commit }) {
        const rgb = {
            r: 51,
            g: 51,
            b: 51
        }
        let parmes = {
            theme: 'black',
            color: '#333333',
            rgb: rgb
        };
        await appSetting().then(res => {
            // console.debug("请求主题设置结果：", res);
            if (res.status != 1) {
                return;
            }

            if (!res.result) {
                return;
            }

            parmes = res.result;
            parmes.theme = res.result.theme
            parmes.color = res.result.color
            parmes.categorySetting = res.result.categorySetting
            parmes.rgb = colorRgb(res.result.color)
        }).finally(e => {
            // console.error(e);
        })
        commit('setThemes', {
            themes: parmes
        })
    },
    /* 
     * 获取服务器上的当前时间与机器上的当前时间的时间差
     * 用法:  this.$store.dispatch('serverNowDate'); 
    */
    async serverNowDate({ commit }) {
        let parmes = 0;
        
        const getTime = new Date().getTime()
        
        await serverNowDate().then(res => {
            if (res.status != 1) {
                return;
            }
            if (!res.result) {
                return;
            }
            parmes = new Date().getTime() - (new Date().getTime() - getTime + res.result) // 要加上服务器的请求时间
        }).finally(e => {
            // console.error(e);
        })
        commit('setTimeDiff', parmes)
    },
    /* 
     * 获取用户信息
     * 用法:  this.$store.dispatch('getUserInfo'); 
    */
    async getUserInfo({ commit }) {
        let parmes = 0;
        await getUserInfo().then(res => {
            if (res.status != 1) {
                return;
            }
            if (!res.result) {
                return;
            }
            parmes = res.result
        }).finally(e => {
            // console.error(e);
        })
        commit('setUserInfo', parmes)
    },
};

var colorRgb = function(sColor) {
    sColor = sColor.toLowerCase();
    // 十六进制颜色值的正则表达式
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
    // 如果是16进制颜色
    if (sColor && reg.test(sColor)) {
        if (sColor.length === 4) {
            var sColorNew = '#';
            for (var i = 1; i < 4; i += 1) {
                sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
            }
            sColor = sColorNew;
        }
        // 处理六位的颜色值
        var sColorChange = [];
        for (var i = 1; i < sColor.length; i += 2) {
            sColorChange.push(parseInt('0x' + sColor.slice(i, i + 2)));
        }

        return {
            r: sColorChange[0],
            g: sColorChange[1],
            b: sColorChange[2]
        };
    }
    return sColor;
};
