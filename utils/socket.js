import config from '@/config/config.js';

class Socket {
  /* websocket实例 */
  ws = null

  /* '_'为私有属性，外部不可调用 */

  /* 状态 */

  // 连接状态
  _alive = false

  // 把类的参数传入这里，方便调用
  _params = null


  /* 计时器 */
  // 重连计时器
  _reconnect_timer = null
  // 心跳计时器
  _heart_timer = null

  /* 参数 */

  // 心跳时间 30秒一次
  heartBeat = 30000
  // 是否自动重连
  reconnect = true
  // 重连间隔时间
  reconnectTime = 5000
  // 重连次数
  reconnectTimes = 10

  /* 初始化 */
  init() {
    clearInterval(this._reconnect_timer)
    clearInterval(this._heart_timer)

    if ('WebSocket' in window) {
      this.ws = null
      this.ws = new WebSocket(config.imServer)
    } else {
      this.$toast('"当前浏览器 Not support websocket"')
    }



    // 默认绑定事件
    this.ws.onopen = () => {
      // 设置状态为开启
      this._alive = true
      clearInterval(this._reconnect_timer)
      // 连接后进入心跳状态
      this.onheartbeat()
      console.log('WebSocket连接成功,状态：' + this.ws.readyState + '  成功时间：' + new Date());
    }

    

    this.ws.onclose = () => {
      console.log('WebSocket连接关闭,状态：' + this.ws.readyState);
      console.log('连接关闭时间：' + new Date());

      // 设置状态为断开
      this._alive = false
      clearInterval(this._heart_timer)

      // 自动重连开启  +  不在重连状态下

      if (this.reconnect == true) {
        console.log('重连中！！！！')
        /* 断开后立刻重连 */
        this.onreconnect()
      }
    }
  }

  /* 心跳事件 */
  onheartbeat(func) {
    // 在连接状态下
    if (this._alive == true) {
      /* 心跳计时器 */
      this._heart_timer = setInterval(() => {
        // 发送心跳信息
        console.log('心跳------' + new Date());
        func ? func(this) : false
      }, this.heartBeat);
    }
  }

  /* 重连事件 */
  onreconnect(func) {
    /* 重连间隔计时器 */
    this._reconnect_timer = setInterval(() => {
      // 限制重连次数
      if (this.reconnectTimes <= 0) {
        // 关闭定时器
        clearInterval(this._reconnect_timer)
        // 跳出函数之间的循环
        return
      } else {
        this.reconnectTimes--
      }
      // 进入初始状态
      this.init()
      func ? func(this) : false
    }, this.reconnectTime);
  }

  /*
   *
   * 对原生方法和事件进行封装
   *
   */

  // 发送消息
  send(text) {
    if (this._alive == true) {
      text = typeof text === 'string' ? text : JSON.stringify(text)
      this.ws.send(text)
    } else {
      this.$toast('发送信息失败')
    }
  }

  // 断开连接
  close() {
    if (this._alive == true) {
      this.reconnect = false
      this._alive = false
      clearInterval(this._heart_timer)
      clearInterval(this._reconnect_timer)
      clearInterval(this.heartBeat)
      clearInterval(this.reconnectTime)
      this.ws.close()
      console.log('WebSocket连接关闭,状态：' + this.ws.readyState);
    }
  }

  // 接受消息
  onmessage(func,all = false) {
    this._alive = true
    clearInterval(this._reconnect_timer)
    // 连接后进入心跳状态
    this.onheartbeat()
    this.ws.onmessage = data => func(!all ? data.data : data)
  }

  // websocket连接成功事件
  onopen(func) {
    this.ws.onopen = event => {
      this._alive = true
      func ? func(event) : false
    }
  }

  // websocket关闭事件
  onclose(func) {
    this.ws.onclose = event => {
      // 设置状态为断开
      this._alive = false
      clearInterval(this._heart_timer)
      // 自动重连开启  +  不在重连状态下
      if (this.reconnect == true) {
        /* 断开后立刻重连 */
        this.onreconnect()
      }
      func ? func(event) : false
    }
  }

  // websocket错误事件
  onerror(func) {
    this.ws.onerror = event => {
      func ? func(event) : false
    }
  }
}




export default Socket






