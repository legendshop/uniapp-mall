import config from '@/config/config.js';

import { appToken } from '@/utils/Cache.js'


// const upload = '/uploadFile';   //上传图片接口
let upload = ''
if (!config.isOauth2) { // 如果不是微服务版本(即是正常版本)
    upload = '/p/upload'; // 上传图片接口
}else{ // 如果是微服务版本
    upload = '/portalapp/p/upload'; // 上传图片接口
}

const IMG_SIZE = 5; // 5m的图片大小

class Uploader {
	constructor() {

	}
	// 选择图片   参数 num 为要选择的图片数量
	choose(num) {
		return new Promise((resolve, reject) => {
			uni.chooseImage({
				count: num,
				sizeType: ['compressed'],
				success(res) {
					// 缓存文件路径
					for (let i = 0; i < res.tempFiles.length; i++) {
						const element = res.tempFiles[i]
						if (element.size / 1024 / 1024 > IMG_SIZE) {
							reject('上传图片不能大于5M')
						} else {
							resolve(res.tempFilePaths)
						}
					}
				}
			})
		}).catch((err) => {
			uni.hideLoading()
			uni.showToast({ icon: 'none', title: err });
			return false
		})
	}
	/* 上传图片 
        num:每次最大可以选择上传的图片数量
        toastSucceed：上弹成功后，是否弹出'上传成功'提示
    */
	choose_and_upload(num,toastSucceed = true) {
        // #ifdef H5
        /* H5只能单选(num = 1)
           count 值在 H5 平台的表现，基于浏览器本身的规范。目前测试的结果来看，只能限制单选/多选，并不能限制数量。并且，在实际的手机浏览器很少有能够支持多选的。
        */
        // num = 1
        // #endif
		// console.log(num)
		return new Promise(async(resolve, reject) => {
			const path_arr = await this.choose(num);
            
			if(!path_arr) {
				return
			}
			
			let img_urls = await this.upload(path_arr,toastSucceed);
            
            if (img_urls && img_urls.length && (!img_urls[0] || img_urls[0] == 'FORMAT_ERR')) { // 如果返回的是一个空的数组：[]，或者upload接口报错
                img_urls = []
            }
            
            // console.log(111111);
            // console.log(img_urls);
            // console.log(img_urls.length);
            // console.log(img_urls[0]);
            
			resolve(img_urls);
		})
	}
	

	// 上传 参数 path 选择成功后返回的 缓存文件图片路径
	uploadOne(path,toastSucceed) {
		// 获取认证的头
		const authHeaders = appToken.generateAuthHeaders()
		
		
		const header = {
			// 'Content-Type': 'multipart/form-data'
		} 
		
        if (!config.isOauth2) { // 如果不是微服务版本(即是正常版本)
            header.accessToken = authHeaders.accessToken
            header.securiyCode = authHeaders.securiyCode
            header.sign = authHeaders.sign
            header.time = authHeaders.time
            header.userId = authHeaders.userId
            header.verId = authHeaders.verId
            header.platform = authHeaders.platform
        }else{ // 如果是微服务版本
            // console.log(11111);
            // console.log(authHeaders);
            header.Authorization = authHeaders.Authorization
        }
		
		return new Promise((resolve, reject) => {
			uni.showLoading({
				title: '上传中'
			})
			// console.log(111111)
			// console.log(config.server)
            console.log(path)
			uni.uploadFile({
				url: config.server + upload, // 仅为示例，非真实的接口地址
				filePath: path,
				name: 'files',
				header: header,
				// formData: {'user': 'test'},
				success:(uploadFileRes) => {
                    // console.log(3333333);
					if (typeof uploadFileRes.data === 'string') {
						if(JSON.parse(uploadFileRes.data).result) {
							resolve(JSON.parse(uploadFileRes.data).result)
							if (toastSucceed) {
							    uni.showToast({ title: '上传成功' });
							}
						} else {
							reject(JSON.parse(uploadFileRes.data).msg)
						}
					} else {
						if(uploadFileRes.data.result) {
							resolve(uploadFileRes.data.result)
							if (toastSucceed) {
							    uni.showToast({ title: '上传成功' });
							}
						} else {
							reject(uploadFileRes.data.msg)
						}
					}
				},
				fail: () => {
					uni.showToast({
						icon: 'none',
					    title: '上传失败'
					});
				},
				complete:() => {
					uni.hideLoading()
				}
				
			});
		}).catch((res) => {
			uni.showToast({ icon: 'none', title: res });
			return false
		})
	}
	
	// 上传多张图片 参数 path_arr 选择图片成功后 返回的图片路径数组
	upload(path_arr,toastSucceed) {
		const num = path_arr.length;
		return new Promise(async(resolve, reject) => {
			const img_urls = []
			for (let i = 0; i < num; i++) {
				const img_url = await this.uploadOne(path_arr[i],toastSucceed);
				console.log(img_url)
				img_urls.push(img_url[0])
			}
            // console.log(img_urls)
			resolve(img_urls)
		})
	}
}


export default Uploader;
