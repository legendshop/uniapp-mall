// data=[{type:'number',value:'fkdfj',messages:'fdsfadsf'}]
const checkinput = (data) => {
    let state = true
    for(const i in data){
        switch (data[i].type) {
            case 'number': 
                state = /^[0-9]*$/.test(data[i].value) 
                break;
            case 'phone':// 手机号码 (  /^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}$/:旧的,已验证不行  )
                state = /^1[0-9][0-9]{9}$/.test(data[i].value) 
                break;
            case 'username':// 用户名:2~20位字母、数字和_，不能以数字和_开头
                state = /^[a-zA-Z]\w{1,19}$/.test(data[i].value) 
                break;
            case 'int':// 整数
                state = /^-?\d+$/.test(data[i].value) 
                break;
            case 'payPwd':// 支付密码(允许纯数字):支付密码由6-20位字母、数字或符号(除空格)组成
                state = /^[\x21-\x7E]{6,20}$/.test(data[i].value) 
                break;
            case 'email':
                state = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test(data[i].value) 
                break;
            case 'isrequire':
                state = data[i].value || null
                break;
            default:
                state = false
            break;
        }
        if(!state){
            if (data[i].messages) {
                uni.showToast({ title:data[i].messages,duration: 2000,icon:'none' })
            }
            return false;
        }
    }
    return true
}
export default checkinput

/* 用法:
<script>
    import checkInfo from '@/utils/checkInfo.js';
    
    methods: {
        sure(){
            if (!checkInfo([{ type: 'username', value: this.newAccount.userName, messages: '用户名需要为2~20位字母、数字和_，不能以数字和_开头' }])) {
                return
            }
            if(!checkInfo([
            	{type:'username',value:userName,messages:'用户名需要为字母、数字和_，不能以数字和_开头'},
            	{type:'isrequire',value:shopName,messages:'店铺名称不能为空'},
            	{type:'isrequire',value:province,messages:'店铺地址不能为空'},
            ])){
            	return
            }
        },
    }
 */
