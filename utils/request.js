import axios from 'uni-axios';
import config from '@/config/config.js';
import configfix from '@/config/configfix.js';
import { pages,getUa,debounce } from '@/utils/utils.js';
import { appToken,passport } from '@/utils/Cache.js';
import oauthService from '@/service/oauth.js';

axios.defaults.timeout = 6000; // 请求超时时间
axios.defaults.baseURL = config.server;
axios.defaults.withCredentials = true;

const CancelToken = axios.CancelToken;
let cancel = null;

// 请求拦截器
axios.interceptors.request.use(
    options => {
        return options
    },
    error => {
        // console.log(error)
        return Promise.reject(error);
    });


// 响应拦截器
axios.interceptors.response.use(
    response => {
        // console.log(response)
        if (response.status === 200) {
            if (response.data.status) {
                if (response.data.status == 401) {
                    _method.goLogin()
                }
            }
            return Promise.resolve(response);
        } else {
            uni.showToast({
                title: '网络请求异常, 请检查您的网络设置后刷新重试!',
                duration: 1500,
                mask: true,
                icon: 'none'
            });
            return Promise.reject(response);
        }
    },
    // 服务器状态码不是200的情况
    error => {
        if (error && error.response && error.response.status) {
            // console.log('2~~~~~~~~~'+error);
            switch (error.response.status) {
                // 401: 未登录
                case 401:
                    _method.checkToken()
                    break;
                case 400:// refreshToken时请求头验证不通过
                    if (config.isOauth2) { // 微服务报400错误时才checkToken，正常版本报400直接不处理
                        _method.checkToken()
                    }
                    break;
                case 403:
                    uni.showToast({ title: '您没有访问权限', duration: 1000, mask: true, icon: 'none' });
                    uni.reLaunch({ url: '/pages/authority/authority' });
                    break;
                case 426:// 微服务版本才有
                    uni.showToast({ title: '密码错误！', duration: 1000, mask: true, icon: 'none' });
                	break;
                case 428:// 微服务版本才有
                    uni.showToast({ title: '验证码错误', duration: 1000, mask: true, icon: 'none' });
                	break;
                default:
                    uni.showToast({ title: '网络请求异常, 请检查您的网络设置后刷新重试!', icon: 'none', duration: 1500, icon: 'none' })
                    break;
            }
        } else {
            // console.log('1~~~~~~~~~'+error);
            uni.showToast({ title: '网络请求异常, 请检查您的网络设置后刷新重试!', icon: 'none', duration: 1500, icon: 'none' })
        }
        return Promise.reject(error);
    }
)

const request = {
    /**
     * get方法
     * @param url 请求url, 相对url
     * @param params 请求参数
     * @param config 扩展配置
     */
    get(url, params, opt) {
        return this.sendRequest('GET', url, params, opt);
    },

    post(url, params, opt) {
        return this.sendRequest('POST', url, params, opt);
    },

    PUT(url, params, opt) {
        return this.sendRequest('PUT', url, params, opt);
    },
    /**
     * 通用的请求方法
     * @param method 请求method
     * @param url 请求url
     * @param data 请求参数
     * @param header 请求url
     * @param options 其他选项
     */
    sendRequest(method, url, data, options = {}) {
        // 定义一些默认的请求参数, 请求方法的options参数

        // headers: {
        // 	"Content-type": "application/x-www-form-urlencoded",
        // 	"aaa": "123123123"
        // }

        const headers = {}
        if (options.isJson) {
            headers['Content-Type'] = 'application/json'
        } else {
            headers['Content-Type'] = 'application/x-www-form-urlencoded'
        }
        
        if (config.isOauth2) { // 如果是微服务版本
            if (_method.isNeedGetway(url)) {
            	if (_method.isOauthRequest(url)) {
            		url = config.server + configfix.getway.oauthPrefix + url;
            	} else {
            		url = config.server + configfix.getway.prefix + url;
            	}
            } else {
            	url = config.server + url
            }
        }
        
        const defaultOptions = {
            isShowLoadding: false, // 是否显示loadding,默认否
            isLoaddingMask:true,// 当isShowLoadding==true时，是否显示遮罩层(这时候不能点击页面，防止重复点击或接口未请求完就点击按钮走下一步)
            isShowToast: true, // 是否显示当res.status != 1时的showToast，默认：显示
            loaddingMsg: '加载中...', // 默认loadding提示语
            url: config.isOauth2 ? url : config.server + url,// 如果是微服务，就用上面的得出的url地址，如果不是微服务，就直接用config.server + url
            method: method,
            headers: headers,
            // 	transformRequest: [
            // 		function(data) {
            // 		 //  if (!options) {
            // 			// data = QS.stringify(data);
            // 		 //  } else if (!options.headers) {
            // 			// data = QS.stringify(data);
            // 		 //  } else {
            // 			// let contentType = options.headers["Content-Type"];
            // 			// if (!contentType || contentType.indexOf("application/x-www-form-urlencoded") != -1) {
            // 			//   data = QS.stringify(data);
            // 			// }
            // 		 //  }

            // 		  return QS.stringify(data);
            // 		}
            // 	],
            // paramsSerializer: function(params) {
            // 	return QS.stringify(params);
            // }
        };

        if (method == 'GET' || method == 'PUT') {
            defaultOptions.params = data;
        } else {
            defaultOptions.data = data;
        }

        if (options) {
            options = { ...defaultOptions, ...options };
            if (config.isOauth2) { // 微服务版本才用到里面的代码
                if(options.headers){
                	options.headers = { ...options.headers,...headers }
                }
            }
        } else {
            options = defaultOptions;
        }

        // console.log(options)

        // 应用opt
        if (options.isShowLoadding) {
            uni.showLoading({
                title: options.loaddingMsg,
                mask: options.isLoaddingMask
            });
        }

        // 如果需要token
        if (_method.isNeedToken(url)) {
            // 获取认证的头
            const authHeaders = appToken.generateAuthHeaders()
            // console.log('进来了')
            // console.log(authHeaders)
            if (authHeaders) {
                // 判断是否有认证头, 如果有则给每个HTTP都加上
                options.headers = { ...authHeaders, ...options.headers } // 让每个请求携带自定义token 请根据实际情况自行修改
                // console.log(options.headers)
            } else {
                if (appToken.isNeedAuth(url)) { // 说明没有token, 但又需要认证
                    if (options.isShowLoadding) {
                        uni.hideLoading();
                    }
                    return new Promise((resolve, reject) => {
                        _method.goLogin()
                    });
                }
            }
        }
        
        return new Promise((resolve, reject) => {
            axios.request(options).then(res => {
                if(res.data.status == 401)return// 如果Status Code: 200,但却是res.data.status=401未登陆状态，则把res拦截下来，不让页面toast:您的请求被拒绝 请登入后重试(因为当401时，页面已经showModle了)
                if(res.data.status != 1 && options.isShowToast){ // 这里统一处理当res.status != 1时的将 res.msg Toast出来
                    uni.showToast({ title: res.data.msg, duration: 2000, icon: 'none' })
                }
                resolve(res.data);
            }).catch(err => {
                reject(err);
            }).finally(res => {
                if (options.isShowLoadding) {
                    uni.hideLoading();
                }
                cancel = null;
            });
        });
    }
}




// 私有的方法
const _method = {
    /**
     * 判断url是否需要带token, 临时的用法, 后续要改造
     * @param {*} url
     */
    isNeedToken(url) {
        // 如果被排除
        if (configfix.excludeTokenURIs && configfix.excludeTokenURIs.length) {
            for (let i = 0, len = configfix.excludeTokenURIs.length; i < len; i++) {
                const uri = configfix.excludeTokenURIs[i];
                if (url.indexOf(uri) > 0) {
                    return false;
                }
            }
        }
        return true;
    },
    
     /** 微服务版本专用
     * 判断url是否需要过网关, 对于微服务版本需要过网关
     * @param {*} url
     */
    isNeedGetway(url) {
    	// 如果被排除
    	if (configfix.getway.excludeURIs && configfix.getway.excludeURIs.length) {
      		for (let i = 0, len = configfix.getway.excludeURIs.length; i < len; i++) {
    			const uri = configfix.getway.excludeURIs[i];
    			if (url.indexOf(uri) != -1) {
    				return false;
    			}
    		}
    	}
    
    	return true;
    },
    /** 微服务版本专用
     * 是否是认证的请求
     * @param {} url
     */
    isOauthRequest(url) {
    	if (url.indexOf('/oauth') === -1) {
    		return false;
    	}
    
    	return true;
    },
    /**
     * 清除本地token和清空vuex中token对象
     * 去往登录页面
     */
    goLogin() {
        // 清除token
        appToken.clearToken()
        uni.hideToast()
        appToken.goLogin()
    },
    /**
     * 验证请求头，
     * 去往登录页面
     */
    checkToken:debounce(function(){
        if (config.isOauth2 && appToken.getToken() && !uni.getStorageSync('dealCheckToken')) { // 微服务版本才有checkToken接口,dealCheckToken是为了防止每个接口都请求一次checkToken
            oauthService.checkToken(appToken.getToken().accessToken).then(res => {
                if (res.success) {
                    if (config.isOauth2 && appToken.getToken().refreshToken) { // 如果Token已过期，并且缓存里有refreshToken,则重新刷新token登录。只有微服务才有refreshToken
                        oauthService.refreshToken(appToken.getToken().refreshToken).then(res => {
                            if (res.success) {
                                passport.clearPassportIdKey();// 登录成功后不清除掉passportId的话，退出登录后，下次登录时，就会取上一个账号的passportId，这样就会导致passportId对不上而登录失败
                                uni.showToast({ title:'登录成功',mask : true,
                                	complete() {
                                		setTimeout(() => {
                                            uni.navigateBack({ delta:0 })
                                		}, 1500);
                                	}
                                })
                            } else {
                                this.goLogin();
                                // uni.showToast({ title: res.msg,icon:'none',duration: 2000 });
                            }
                        });
                    }else{
                        this.goLogin();
                    }
                } else {
                    this.goLogin();
                    // uni.showToast({ title: res.msg,icon:'none',duration: 2000 });
                }
            }).catch(e => { // '请求头验证不通过!';
                // console.log(e);
                this.goLogin();
                // uni.showModal({ title: '温馨提示',content: '请求头验证不通过，可以尝试请空缓存，确定要清空缓存吗？',success: function(res) {
                //     if (res.confirm) {
                //         uni.setStorageSync('dealCheckToken', true);
                //         uni.clearStorageSync();
                //         uni.navigateBack({ delta:0 })//注意，必需要先清缓存再跳回本页
                //     } else if (res.cancel) {
                        
                //     }
                // } });
            }).finally(res => {
                uni.setStorageSync('dealCheckToken', true);
            });
        }else{
            this.goLogin();
        }
    },1000),
}


export default request;
