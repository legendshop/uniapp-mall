
// text 要传文本
// digits 默认是2位小数点
function cash(text = 0, digits = 2) {
    if(!text) {
      text = 0
    }
    if(!(text instanceof Number)){
      text = Number(text);
    }

    text = (text.toFixed(digits)).toString()
    if (text.indexOf('.') == -1) {
        text = text + '.00'
    }
    return text.indexOf('.') != -1 ? text.split('.') : text.join('');
}


export default cash
