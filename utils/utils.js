import stringUtils from '@/utils/stringUtils.js'
import configfix from '@/config/configfix.js'
var lodash = require('lodash');

/**
 * 获取用户浏览器UA
 */
const getUa = function() {
    const _cache = {};
    if (_cache.ua) {
        return _cache.ua;
    }

    let isMobile = false;
    let isApple = false;
    let isAndroid = false;
    let isUc = false;
    let isWeixin = false;
    let isQQ = false;
    let isQQBrowser = false;
    let isWeibo = false;

    // #ifdef H5
    // APP端可用，plus.navigator.getUserAgent
    var ua = window.navigator.userAgent.toLocaleLowerCase();
    // console.log(ua);
    isMobile = !!ua.match(/AppleWebKit.*Mobile.*/i);
    isApple = !!ua.match(/(ipad|iphone|ipod|mac)/i);
    isAndroid = !!ua.match(/android/i);
    isUc = !!ua.match(/ucbrowser/i);
    isWeixin = !!ua.match(/micromessenger/i);// 微信内置浏览器
    isQQ = !!ua.match(/pa qq/i);// QQ内置浏览器
    isQQBrowser = !!ua.match(/MQQBrowser/i);// QQ浏览器
    isWeibo = !!ua.match(/weiBo/i);
    // #endif

    _cache.ua = {
        isMobile: isMobile,
        isApple: isApple,
        isAndroid: isAndroid,
        isUc: isUc,
        isWeixin: isWeixin,
        isQQ: isQQ,
        isWeibo: isWeibo,
        isQQBrowser:isQQBrowser
    };

    return _cache.ua;
};


/**
 * 处理时间戳的工具
 */
const time = {
    /**
     * 传送一个以前或将来的时间戳，计算现在距离这个时间多久，小于小时的返回:X分钟前(后)，大小于1天的返回：X小时前(后).......
     */
    timeAgo(time) {
        var nowTime = new Date().getTime();
        var f = nowTime - time;
        var bs = (f >= 0 ? '前' : '后'); // 判断时间点是在当前时间的 之前 还是 之后
        f = Math.abs(f);
        if (f < 6e4) {
            return '刚刚'
        } // 小于60秒,刚刚
        if (f < 36e5) {
            return parseInt(f / 6e4) + '分钟' + bs
        } // 小于1小时,按分钟
        if (f < 864e5) {
            return parseInt(f / 36e5) + '小时' + bs
        } // 小于1天按小时
        if (f < 2592e6) {
            return parseInt(f / 864e5) + '天' + bs
        } // 小于1个月(30天),按天数
        if (f < 31536e6) {
            return parseInt(f / 2592e6) + '个月' + bs
        } // 小于1年(365天),按月数
        return parseInt(f / 31536e6) + '年' + bs; // 大于365天,按年算
    },
}

const pages = {
    /* 从什么页面过来的就跳回什么页面去:
        num: Number,跳转到前num个页面,默认值为0,传1就是跳转到前一个页面,传2就是跳转到前2个页面,0是当前页面。当没有前一个页面时,则默认跳到首页
        excludePage:Array,排除某个页面(当检测到该页面包含该符时的页面时,直接跳到首页),如：pages.goBeforePage(1,['/login','thirdLoginResult']),
        备注：如果传的excludePage数组第一个元素是传['login']，则会触发条件，排除configfix.excludeLoginURIs这些页面，如：pages.goBeforePage(1,['login'])
    */
    goBeforePage(num, excludePage = []) {
        const pages = getCurrentPages(); // 当前页面
        const pagesNumber = num ? Number(num) : 0
        const beforePage = pages[pages.length - pagesNumber - 1]; // 前num个页面

        let isExclude = false
        if (excludePage[0] == 'login') { // 如果是登录后的从什么页面过来的就跳回什么页面去，则configfix.excludeLoginURIs这些页面是直接跳首页而不是返回
            excludePage = configfix.excludeLoginURIs
        }
        for (var i = 0; i < excludePage.length; i++) {
            if (beforePage && beforePage.route.indexOf(excludePage[i]) != -1) { // 如果不先判断beforePage，会报没有route的bug
                isExclude = true
            }
        }

        if (!beforePage || (excludePage && isExclude)) {
            // 跳转首页
            uni.switchTab({
                url: '/pages/index/index'
            });
        } else {
            // 从什么页面过来的就跳回什么页面去
            if (pagesNumber == 1) {
                uni.navigateBack()
            } else {
                uni.navigateBack({
                    delta: pagesNumber
                });
                // 注意,如果返回带参数的页面,不能用该方法,因为该方法并不能带上参数的
                // uni.reLaunch({url: '/' + beforePage.route})
            }
        }
    },
    /* 拿当前页面的路由地址(注意:不包括参数)
        num: Number,拿前N个页面的路由地址,默认值为0,传1拿前一个页面的路由地址
    */
    getPageRoute(num) {
        const pagesNumber = num ? Number(num) : 0
        const pages = getCurrentPages(); // 当前页面
        const beforePage = pages[pages.length - pagesNumber - 1]; // 前num个页面,0是当前页面
        return beforePage ? beforePage.route : false
    },
}
/* 防抖
 *@param fn {Function}   实际要执行的函数
 * @param delay {Number}  延迟时间，也就是阈值，单位是毫秒（ms）
 * @return {Function}     返回一个“去弹跳”了的函数
 * 用法：import { debounce } from '@/utils/utils.js';
 *       sure:debounce(function(){}),
 */
function debounce(fn, delay = 500) {
    let timer = null; // 声明计时器
    return function() {
        const context = this;
        const args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function() {
            fn.apply(context, args);
        }, delay);
    };
}


/**
 * 处理图片的工具
 */
const image = {
    /** 图片转base64格式(现在只有小程序上有用)
     *  @param  {img} :图片路径，必须是string，不然会报错
     * 例子：image.base64Img('/static/images/defalut-img/head-default.jpg')，得出的是data:image/png;base64,*********的图片
     */
    base64Img(img) {
        let suffix = ''
        // #ifdef MP
        // 由于小程序端不在海报里用本地图片(必须要先转base64),这里先转出图片为base64
        // let adfsdf = '/static/images/defalut-img/head-default.jpg'
        // 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="300" height="225"/>',
        // src: 'data:image/svg+xml;utf8,' + src.replace(/#/g, '%23'),
        // <image src="data:image/png;base64,iVBORw0KGgoARK5CYII=" mode="widthFix"></image>
        const imgData = uni.getFileSystemManager().readFileSync(img, 'base64');
        const base64 = '' + imgData;
        suffix = stringUtils.getSuffix(img);
        let base64Img = ''
        if (suffix == 'svg') {
            base64Img = `data:image/${suffix}+xml;base64,${base64}`
        } else {
            base64Img = `data:image/${suffix};base64,${base64}`
        }
        return base64Img
        // #endif

        // #ifdef H5
        suffix = stringUtils.getSuffix(img);
        return img
        // #endif

        // #ifdef APP-PLUS
        // let canvas = document.getElementById('myCanvas');
        // let ImgBase64 = canvas.toDataURL('image/png');

        // let svg = document.getElementById(img);
        // let s = new XMLSerializer().serializeToString(svg);
        // let ImgBase64 = `data:image/svg+xml;base64,${window.btoa(s)}`;
        // #endif
    },
    /**
     * 
     * @param {*} imageUrl 
     * @param {*} type 指定宽高 wh 还是按比例缩放 scale
     * @param {*} options 按比例的时候接收 scale
     */
    handleResize(imageUrl, type, options) {
        let params = `${imageUrl}?x-oss-process=image/resize,`;
        if (type === 'wh') {
            for (const key in options) {
                switch (key) {
                    case 'm':
                        params = `${params}m_${options[key]},`
                        break;
                    case 'w':
                        params = `${params}w_${options[key]},`
                        break;
                    case 'h':
                        params = `${params}h_${options[key]},`
                        break;
                    case 'l':
                        params = `${params}l_${options[key]},`
                        break;
                    case 's':
                        params = `${params}s_${options[key]},`
                        break;
                    case 'limit':
                        params = `${params}limit_${options[key]},`
                        break;
                    case 'color':
                        params = `${params}color_${options[key]},`
                        break;
                    default:
                        break;
                }
            }
            params = params.substr(0, params.length - 1);
        } else if (type === 'scale') {
            params = `${params}p_${options.scale}`
        }
        return params
    },

    /**
     *  检测输入的内容是否为空
     * @param {Object} input:传送进来的文本框里输入的字符串，如果没有输入内容，则返回：true
     * 使用：
     *  import { image } from '@/utils/utils.js';
     *  image.pathToBase64(path).then(res => {
            console.log(res)
        }).catch(error => {
            console.error(error)
        })
     */
    pathToBase64(path) {
        return new Promise(function(resolve, reject) {
            // if (typeof window === 'object' && 'document' in window) {
            // #ifdef H5
            if (typeof FileReader === 'function') {
                var xhr = new XMLHttpRequest()
                xhr.open('GET', path, true)
                xhr.responseType = 'blob'
                xhr.onload = function() {
                    if (this.status === 200) {
                        const fileReader = new FileReader()
                        fileReader.onload = function(e) {
                            resolve(e.target.result)
                        }
                        fileReader.onerror = reject
                        fileReader.readAsDataURL(this.response)
                    }
                }
                xhr.onerror = reject
                xhr.send()
                return
            }
            var canvas = document.createElement('canvas')
            var c2x = canvas.getContext('2d')
            var img = new Image()
            img.onload = function() {
                canvas.width = img.width
                canvas.height = img.height
                c2x.drawImage(img, 0, 0)
                resolve(canvas.toDataURL())
                canvas.height = canvas.width = 0
            }
            img.onerror = reject
            img.src = path
            return
            // #endif
            // }
            // if (typeof plus === 'object') {
            // #ifdef APP-PLUS
            plus.io.resolveLocalFileSystemURL(getLocalFilePath(path), function(entry) {
                entry.file(function(file) {
                    var fileReader = new plus.io.FileReader()
                    fileReader.onload = function(data) {
                        resolve(data.target.result)
                    }
                    fileReader.onerror = function(error) {
                        reject(error)
                    }
                    fileReader.readAsDataURL(file)
                }, function(error) {
                    reject(error)
                })
            }, function(error) {
                reject(error)
            })
            return
            // #endif
            // }
            // if (typeof wx === 'object' && uni.canIUse('getFileSystemManager')) {
            // #ifdef MP
            uni.getFileSystemManager().readFile({
                filePath: path,
                encoding: 'base64',
                success: function(res) {
                    const suffix = stringUtils.getSuffix(path);
                    if (suffix == 'svg') {
                        resolve(`data:image/${suffix}+xml;base64,` + res.data)
                    } else {
                        resolve(`data:image/${suffix};base64,` + res.data)
                    }
                    // resolve('data:image/png;base64,' + res.data)
                },
                fail: function(error) {
                    reject(error)
                }
            })
            return
            // #endif

            // }
            // reject(new Error('not support'))
        })
    },

    base64ToPath(base64) {
        return new Promise(function(resolve, reject) {
            // if (typeof window === 'object' && 'document' in window) {
            // #ifdef H5
            base64 = base64.split(',')
            var type = base64[0].match(/:(.*?);/)[1]
            var str = atob(base64[1])
            var n = str.length
            var array = new Uint8Array(n)
            while (n--) {
                array[n] = str.charCodeAt(n)
            }
            return resolve((window.URL || window.webkitURL).createObjectURL(new Blob([array], {
                type: type
            })))
            // #endif
            // }
            var extName = base64.match(/data\:\S+\/(\S+);/)
            if (extName) {
                extName = extName[1]
            } else {
                reject(new Error('base64 error'))
            }
            var fileName = Date.now() + '.' + extName
            // if (typeof plus === 'object') {
            // #ifdef APP-PLUS
            var bitmap = new plus.nativeObj.Bitmap('bitmap' + Date.now())
            bitmap.loadBase64Data(base64, function() {
                var filePath = '_doc/uniapp_temp/' + fileName
                bitmap.save(filePath, {}, function() {
                    bitmap.clear()
                    resolve(filePath)
                }, function(error) {
                    bitmap.clear()
                    reject(error)
                })
            }, function(error) {
                bitmap.clear()
                reject(error)
            })
            return
            // #endif
            // }

            // if (typeof wx === 'object' && uni.canIUse('getFileSystemManager')) {
            // #ifdef MP
            var filePath = uni.env.USER_DATA_PATH + '/' + fileName
            uni.getFileSystemManager().writeFile({
                filePath: filePath,
                data: base64.replace(/^data:\S+\/\S+;base64,/, ''),
                encoding: 'base64',
                success: function() {
                    resolve(filePath)
                },
                fail: function(error) {
                    reject(error)
                }
            })
            return
            // #endif
            // }
            // reject(new Error('not support'))
        })
    },
}


/**
 * 处理api请求的工具
 */
const api = {
    /**
     * api：传入的接口
     * params：接口请求的参数
     * options：请求接口前、中、后对数据的处理
     * 用法：在main.js里引入：import { api } from '@/utils/utils.js'; Vue.prototype.$apiRequest = api.request;//封装统一api请求的方法
     *      this.$apiRequest(bindPhone,params,
                {
                    before(){
                        console.log('调用接口前');
                    },
                    success(res){
                        console.log('调用接口成功res.status == 1');
                    },
                    statusError(res){
                        console.log('调用接口成功，但res.status != 1');
                    },
                    error(e){
                        console.log('调用接口失败，有语法上的错误');
                    },
                    complete(res){
                        console.log('调用api处理完成（无论成功或失败）');
                    },
                    showErrorMsg:false,//res.status != 1时，不显示showToast
                }
            )
     */
    request(api, params = {}, options = {}) {
        params = { ...params };
        const defaultOptions = {
        	before: null,// 调用api处理之前
        	success: null,// 调用api成功且 res.status == 1 后
            statusError:null,// 调用api成功且 res.status != 1 后, 要对res.status != 1的情况进行处理时，就把res再暴露出来，在页面上另外处理
        	error: null, // 调用api处理失败
        	complete: null,// 调用api处理完成（无论成功或失败）
            showErrorMsg:true,// res.status != 1时，是否显示showToast
        }
        options = lodash.merge(defaultOptions, options);
        if (options.before) {
        	options.before(); // 空参数调用
        }
        
        api(params).then(res => {
        	if (res.status == 1) {
                if(options.success){
                	options.success(res);
                }
            } else {
                options.statusError(res);
                if (options.showErrorMsg) {
                    uni.showToast({ title: res.msg, duration: 2000, icon: 'none' })
                }
        	}
        }).catch(e => {
			if(options.error){
				options.error(e);
			}
		}).finally(res => {
			if(options.complete){
				options.complete(res);
			}
		})
    }
}

export { time, pages, getUa, debounce, image, api }
