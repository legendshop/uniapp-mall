import config from '@/config/config.js';

/*
 * 应用主题样式

   colorNama : 顔色名字
        主要支持 绿色(green) ， 深红色(crimson) ， 红色(red) ， 浅红色(pale-red) ， 橙色(orange) ， 棕色(brown) ， 蓝色(blue) ， 浅绿(light-green) ， 黑色(black)
 */
function _tabBar(color){
    const themeColor = { '#4eae31':'green', '#de0233':'crimson', '#e5004f':'red','#fd8cb0':'pale-red','#ff6700':'orange','#bc9f5f':'brown','#2e82e3':'blue','#333333':'black','#5fc221':'light-green' };
    const themeColorImg = themeColor[color] ? themeColor[color] : 'black';
    const navigationBar = {
        selectedColor: color || '#333333',
        backgroundColor: color || '#333333',
        list:[
            {
                iconPath: '/static/style-images/index-g.png',
                selectedIconPath: `/static/style-images/index-${themeColorImg}.png`
            },
            {
                iconPath: '/static/style-images/classify-g.png',
                selectedIconPath: `/static/style-images/classify-${themeColorImg}.png`
            },
            {
                iconPath: '/static/style-images/find-g.png',
                selectedIconPath: `/static/style-images/find-${themeColorImg}.png`
            },
            {
                iconPath: '/static/style-images/cart-g.png',
                selectedIconPath: `/static/style-images/cart-${themeColorImg}.png`
            },
            {
                iconPath: '/static/style-images/center-g.png',
                selectedIconPath: `/static/style-images/center-${themeColorImg}.png`
            },
        ],
    }
    if (config.closeDiscovery) { // 如果是这屏蔽了种草功能的，则将数组里的第三个删掉
        navigationBar.list.splice(2,1)
    }
    
    return navigationBar
}

    
// 初始化tabbar风格样式
function initTabBarStyle(color) {
    uni.setTabBarStyle({
        color: '#333333',
        backgroundColor: '#fff',
        borderStyle: 'black',
        selectedColor: color || '#333333',
    })
    
    for (let index = 0; index < _tabBar(color).list.length; index++) {
        const element = _tabBar(color).list[index];
        uni.setTabBarItem({
            index: index,
            iconPath: element.iconPath,
            selectedIconPath: element.selectedIconPath,
            fail: function(error) {
              console.log('更改tabBarItem失败, error: %O', error);
            }
          });
    }
}

// 初始化导航栏风格样式
function initNavigationBarStyle(color) {
    if (color) {
        uni.setNavigationBarColor({
            frontColor: '#ffffff',
            backgroundColor: _tabBar(color).backgroundColor
        })
    }
}

function applyStyle(color,options) {
    // console.log(`color：${color}`)
    // console.log(arguments)
    const defaultOptions = {
        isReload: false,
        isInitTabBar: false,
        isInitNavigationBar: true,
        success: function(res) {
          // console.debug("主题应用成功, res: {}", res);
        },
        fail: function(error) {
          // console.debug("主题应用失败, error: {}", error);
        },
        complate: function() {
        }
    };
    // 合并选项参数
    options = Object.assign(defaultOptions, options);

    // console.log(options)
    if (options.isReload) {
        _method._applyStyle(color,options)
    } else {
        _method._applyStyle(color,options)
    }
}

const _method = {
    _applyStyle(color,options) {
        if (options.isInitNavigationBar) {
            // 初始化页面标题栏风格
            initNavigationBarStyle(color);
        }

        // 初始化tabbar样式
        if (options.isInitTabBar) {
            initTabBarStyle(color);
        }
    }
}

export {
    applyStyle,
    initTabBarStyle,
    initNavigationBarStyle
}

/* 
用法:
    一、main.js里引入全局方法:
        import {applyStyle} from 'utils/colorUtils.js';
        function applyColorStyle(color,options){ applyStyle(color,options) }; Vue.prototype.$applyStyle = applyColorStyle;
        
    二、页面上引用:
        import { applyStyle } from '@/utils/colorUtils.js'; 
        onLoad(option) {
           this.$applyStyle(this.themes.color);
        },
*/
