/**
 * 一些缓存相关的操作方法
 */
import { pages,getUa,debounce } from '@/utils/utils.js';
import config from '@/config/config.js';
import storeVuex from '../store';
import oauthService from '@/service/oauth.js';
import axios from 'uni-axios';
import configfix from '@/config/configfix.js';

const TOKEN_KEY = 'APP_TOKEN'
const OPENID = 'OPENID'
const CONFIG_KEY = 'CONFIG_URL'
const PASSPORT_ID_KEY = 'passportIdKey'
import md5 from 'js-md5'

const SEARCH_KEY = 'searchHistory'
const maxLength = 5;

// 地址搜索管理，这个只有otherModules/changeAddr/changeAddr页面用到
const locationHistory = {

    History: 'History',
    location:  'location',

    // 历史搜索
    getLocationHistory(keyname) {
        // console.log(keyname)
        const that = this
        let words = ''
        if(uni.getStorageSync(keyname)) {
            // console.log(uni.getStorageSync(keyname))
            words = JSON.parse(uni.getStorageSync(keyname))
        }
        if(!words){
            return []
        }
        for (let i = 0; i < words.length; i++) {
            if (words[i] == '' || typeof (words[i]) === 'undefined' || words[i] == '') {
                words.splice(i,1)
                i = i - 1
            }
        }
        return words
    },
    // 添加历史搜索
    addLocationHistory(keyname , keyword) {
        // console.log(keyname)
        const that = this
        let words = that.getLocationHistory(keyname)
        // console.log(words)
        if (Object.prototype.toString.call(words) == '[object String]' || Object.prototype.toString.call(words) == '[object Object]') {
            words = []// 如果words不是数组，则先清空
        }
        const has = words.includes(keyword)

        if (!has) {
            // 数组末尾 删除 ， keyword 数组第一位
            const length = words.length
            if (length >= maxLength){
                words.pop()
            }
            if (Object.prototype.toString.call(words) == '[object String]') { // 如果words不是数组，则先清空
                words = []
            }
            words.unshift(keyword)
            // console.log(words)
            uni.setStorageSync(keyname,JSON.stringify(words))
        }
    },

    // 清除历史搜索缓存
    clearLocationHistory(keyname) {
        uni.removeStorageSync(keyname)
    }
}

// searchList页面上的搜索历史
const searchHistory = {
    getSearchHistory() {
        const that = this
        let words = []
        words = uni.getStorageSync(SEARCH_KEY)
    	// console.log(words)
        if(!words && !words.length){
            return []
        }
        for (let i = 0; i < words.length; i++) {
            if (!words[i]) {
                words.splice(i,1)
            }
        }
        return words
    },
    
    // 添加历史搜索
    addSearchHistory(keyword) {
        const that = this
        let words = that.getSearchHistory()
        if (Object.prototype.toString.call(words) == '[object String]' || Object.prototype.toString.call(words) == '[object Object]') {
            words = []// 如果words不是数组，则先清空
        }
        const has = words.includes(keyword)
        if (!has) {
            // 数组末尾 删除 ， keyword 数组第一位
            const length = words.length
            if (length >= maxLength){
                words.pop()
            }
            words.unshift(keyword)
            // console.log(words)
            uni.setStorageSync(SEARCH_KEY, words)
            // localStorage.setItem(KEY,JSON.stringify(words))
        }
    },
    
    // 清除历史搜索缓存
    clearSearchHistory() {
        uni.removeStorageSync(SEARCH_KEY)
    }
}

const appToken = {
    /**
    	设置token
    	@param {*} result:请求完接口，当res.status == 1时，就把res.result传进来，统一在这里管理登录成功后token值的处理方法
    	@param {*} source:从哪个接口进来的，现在接收dpg的值有：
                          'bindPhone'(在bindingPhone页面请求登录功--有两种情况：一是微信小程序，一是微信浏览器端)
                          'thirdLoginResult'(在thirdLoginResult页面请求登录功，微信浏览器上(getUa().isWeixin==true)登录才用到这个页面)
                          'weixinMpLogin'(在wxLogin页面请求登录功，微信端静默登录成功后，如果返回状态为res.status == 2，则是进bindingPhone页面登录后传进来)
                          'login'(login页面普通版本的用户账号密码及手机快捷登录)
                          'oauthLogin'(APP第三方登录(H5的方法写在login页面上,APP才用到此方法))
                          'oauth2'(login页面普通版本的用户账号密码登录--微服务版本)
    */
    setToken(result,source) {
        let token = {}
        // 如果是bindingPhone页面(微信小程序或微信浏览器端)上登录成功，微信小程序登录成功，APP第三方登录成功
        if (source == 'bindPhone' || source == 'weixinMpLogin' || source == 'oauthLogin') {
            if (config.isOauth2) {
                if (getUa().isWeixin) { // 如果是微信浏览器
                    token = {
                        accessToken: result.value,
                        refreshToken: result.refreshToken.value,
                        tokenType: result.tokenType,
                        expiresIn: result.expiresIn,
                        scope: result.scope && result.scope.length ? result.scope[0] : '',
                        userId: result.additionalInformation.user_id,
                        userName: result.additionalInformation.user_name,
                    };
                }else{ // 如果是微信小程序
                    token = {
                        accessToken: result.value,
                        refreshToken: result.refreshToken.value,
                        tokenType: result.tokenType,
                        expiresIn: result.expiresIn,
                        scope: result.scope,
                        userId: result.additionalInformation.user_id,
                        userName: result.additionalInformation.user_name,
                    };
                }
            }else{
                token = {
                    accessToken: result.accessToken,
                    deviceId: result.deviceId,
                    securiyCode: result.securiyCode,
                    platform: result.platform,
                    userId: result.userId,
                    verId: result.verId
                };
            }
        }
        
        // 如果是从thirdLoginResult页面上登录成功，微信浏览器上(getUa().isWeixin==true)登录才用到这个页面
        if (source == 'thirdLoginResult') {
            result = JSON.parse(result);
            if (config.isOauth2) {
                token = {
                    accessToken: result.value,
                    refreshToken: result.refreshToken.value,
                    tokenType: result.tokenType,
                    expiresIn: result.expiresIn,
                    scope: result.scope && result.scope.length ? result.scope[0] : '',
                    userId: result.additionalInformation.user_id,
                    userName: result.additionalInformation.user_name,
                };
            }else{
                token = {
                    accessToken: result.accessToken,
                    id: result.id,
                    platform: result.platform,
                    securiyCode: result.securiyCode,
                    userId: result.userId,
                    userName: result.userName,
                    verId: result.verId,
                };
            }
        }
        
        // login页面普通版本的用户账号密码及手机快捷登录
        if (source == 'login') {
            // #ifdef MP-WEIXIN
            // 现在微信上已经没有输入账号密码登录了,现在是用weixinMpLogin的静默登录
            token = {
            	accessToken: result.accessToken,
            	securiyCode: result.securiyCode,
            	platform: result.platform,
            	userId: result.userId,
            	verId: result.verId,
            	userName: result.userName,
                id: result.id
             }
            // #endif
            // #ifndef MP-WEIXIN
            token = {
              accessToken: result.accessToken,
              deviceId: result.deviceId,
              securiyCode: result.securiyCode,
              platform: result.platform,
              userId: result.userId,
              verId: result.verId,
              id: result.id
            };
            // #endif
        }
        
        // login页面普通版本的用户账号密码及手机快捷登录--微服务版本
        if (source == 'oauth2') {
            token = {
                accessToken: result.access_token,
                refreshToken: result.refresh_token,
                tokenType: result.token_type,
                expiresIn: result.expirs_in,
                scope: result.scope,
                userId: result.user_id,
                userName: result.user_name
            };
        }
        
    	uni.setStorageSync(TOKEN_KEY, JSON.stringify(token))
    },

	// 获取token
	getToken() {
		const tokenStr = uni.getStorageSync(TOKEN_KEY)
        if (!tokenStr) {
        	return '';
        }
        const token = JSON.parse(tokenStr)
        return token;
	},

	setOpenId(openid){
		uni.setStorageSync(OPENID, JSON.stringify(openid))
	},
	getOpenID() {
		const openidStr = uni.getStorageSync(OPENID)
		if (!openidStr) {
			return null;
		}
	
		const openid = JSON.parse(openidStr)
		return openid;
	},
	
	// 清除token
	clearToken() {
        uni.removeStorageSync(PASSPORT_ID_KEY);// 清除缓存里登录时才储存的passportIdKey(这个只有微信浏览器登录时才用到)
		uni.removeStorageSync(TOKEN_KEY);
        uni.removeStorage('thirdloginHref');// 清除缓存储存的登录页面前一个页面地址(这个只有微信浏览器登录时才用到)
        storeVuex.commit('setUserInfo', {});// 清空VUX里的用户信息
	},
	// 是否登录
	isLogin() {
        const token = this.getToken()
		if (!token) {
			return false;
		}
		return true;
	},
    // 封装的登录方法
    goLogin:debounce(function(){ // 防抖是为了防止小程序端同时弹出多个showModal框(这时要点击多个取消，就算点击了确定还是会弹框)
        const that = this
        uni.showModal({
            title: '登录后才能正常使用噢~',
            content: '请点击“确定"进行登录！',
            success: function(res) {
                if (res.confirm) {
                    that.toLogin()
                } else if (res.cancel) {
                    // 当当前页面的路由出现以下字符时,点击"取消"不作任何反应,而不是返回前一页
                    // 所有详情页面(商品详情，活动详情，文章详情)和个人中心，点击'取消'不作任何反应
                    const stopBackArr = ['Det', 'my/my']
                    let allowBack = true
                    for (var i = 0; i < stopBackArr.length; i++) {
                        if (pages.getPageRoute().indexOf(stopBackArr[i]) != -1) {
                            allowBack = false
                        }
                    }
                    if (allowBack) {
                        uni.navigateBack()
                    }
                    // uni.switchTab({ url: '/pages/index/index' });
                }
            }
        });
    },1000),
    
    toLogin(){
        // #ifdef MP-WEIXIN
        uni.navigateTo({ url: '/accountModules/wxLogin/wxLogin' })
        // #endif
        // #ifndef MP-WEIXIN
        if (getUa().isWeixin) {
            const thirdloginHref = window.location.href;
            uni.setStorageSync('thirdloginHref', thirdloginHref);// 将当前页面放到缓存里，由于微信H5端的第三方登录要经过微信刷新页面，用不了pages.goBeforePage(2)方法返回到当前页面，只能放到缓存里，在thirdLoginResult页面里再取出来用(刻要删掉)
            // 如果是微信浏览器进H5,则调用微信第三方登录(请求这个接口后，后台将会回调到这个页面：accountModules/thirdLoginResult/thirdLoginResult)
            if (!config.isOauth2) { // 如果不是微服务版本(即是正常版本)
                window.location.href = config.server + '/thirdlogin/weixin/h5/authorize';
            }else{ // 如果是微服务版本
                window.location.href = config.server + '/portalapp/thirdlogin/weixin/h5/authorize';
            }
        } else {
            uni.navigateTo({ url: '/accountModules/login/login' })
        }
        // #endif
    },
	// 是否需要授权认证
	isNeedAuth(url) {
		if(url.indexOf('/s/') === -1){
			return false;
		}
		return true;
	},

    // 生成认证头
    generateAuthHeaders() {
        if (!config.isOauth2) { // 如果不是微服务版本(即是正常版本)
            const token = this.getToken();
            if (token) {
            	// 对token进行签名,+token.shopId
            	const sign = md5(token.userId + token.accessToken + token.verId + token.securiyCode);
            	token.sign = sign;
            }
            return token;
        }else{ // 如果是微服务版本
            let headers = null;
            const token = this.getToken();
            if(token){
                headers = { 'Authorization': 'Bearer ' + token.accessToken }
            }
            return headers;
        }
    }
}

/**
 * 通行证 storage 管理
 */
const passport = {
    /**
     * 获取 第三方登录成功后 生成的通行证密钥
     */
    getPassportIdKey(){
        return uni.getStorageSync(PASSPORT_ID_KEY);
    },

    /**
     * 设置 第三方登录成功后 生成的通行证密钥, 设置到sessionStorage
     * @param {*} passportIdKey
     */
    setPassportIdKey(passportIdKey){
        uni.setStorageSync(PASSPORT_ID_KEY, passportIdKey);
    },

    /**
     * 清空第三方登录成功后 生成的通行证密钥
     */
    clearPassportIdKey(){
        uni.removeStorageSync(PASSPORT_ID_KEY);
    }
}


export {
    appToken,
    locationHistory,
    searchHistory,
    passport,
}



